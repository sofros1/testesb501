РезультатОбработки.ClassId = 14;
Данные = Новый Структура;

Данные.Вставить("Ссылка", ОбъектОбработки.Ссылка);
Данные.Вставить("ПометкаУдаления", ОбъектОбработки.ПометкаУдаления);
Данные.Вставить("ЭтоГруппа", ОбъектОбработки.ЭтоГруппа);
Данные.Вставить("Владелец", ОбъектОбработки.Владелец);
Данные.Вставить("Родитель", ОбъектОбработки.Родитель);
Данные.Вставить("Наименование", ОбъектОбработки.Наименование);
Данные.Вставить("ПолноеНаименование", ОбъектОбработки.ПолноеНаименование);
РезультатОбработки.Body = сшпОбщегоНазначения.ПреобразоватьСтруктуруПоФормату(ФорматСообщения, Данные);

РезультатОбработки.ClassId = 158;

Properties = Новый Структура;

Properties.Вставить("access_token", Справочники.АБС_Базис_токен.Access_token.Token);
Properties.Вставить("to_bazis", Истина);
Properties.Вставить("isEmbedRequest", Истина); //Признак того, что это дозапрос (используется на входящей точке Bazis_test чтобы автоматом указать получателя из отправителя)

Для каждого КлЗнч Из ОбъектОбработки Цикл
	Properties.Вставить(КлЗнч.Ключ, КлЗнч.Значение);
КонецЦикла;

Если Не Properties.Свойство("replyMessageClass") Тогда //В каком классе возвращать сообщение
	ВызватьИсключение "В Properties обязательно должно быть свойство 'replyMessageClass'"; 
КонецЕсли;

Если Не Properties.Свойство("messageType") Тогда
	ВызватьИсключение "В Properties обязательно должно быть свойство 'messageType'"; 
КонецЕсли;

РезультатОбработки.Properties = Properties;

РезультатОбработки.Body = сшпОбщегоНазначения.ПреобразоватьСтруктуруПоФормату(ФорматСообщения, ОбъектОбработки);

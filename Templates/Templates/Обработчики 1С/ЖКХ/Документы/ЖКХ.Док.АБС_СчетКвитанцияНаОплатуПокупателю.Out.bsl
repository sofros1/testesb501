РезультатОбработки.ClassId = 195;

Реквизиты = Новый Структура;
Реквизиты.Вставить("Проведен");
Реквизиты.Вставить("Ссылка");
Реквизиты.Вставить("ПометкаУдаления");
Реквизиты.Вставить("Дата");
Реквизиты.Вставить("Номер");
Реквизиты.Вставить("Организация");
Реквизиты.Вставить("Проект");
Реквизиты.Вставить("ЛицевойСчет");
Реквизиты.Вставить("Склад");
Реквизиты.Вставить("ПодразделениеОрганизации");
Реквизиты.Вставить("Контрагент");
Реквизиты.Вставить("ДоговорКонтрагента");
Реквизиты.Вставить("АдресДоставки");
Реквизиты.Вставить("ОрганизацияПолучатель");
Реквизиты.Вставить("СтруктурнаяЕдиница");
Реквизиты.Вставить("ВалютаДокумента");
Реквизиты.Вставить("КратностьВзаиморасчетов");
Реквизиты.Вставить("КурсВзаиморасчетов");
Реквизиты.Вставить("СуммаВключаетНДС");
Реквизиты.Вставить("СуммаДокумента");
Реквизиты.Вставить("ТипЦен");
Реквизиты.Вставить("УдалитьУчитыватьНДС");
Реквизиты.Вставить("РегистрационныйНомер");
Реквизиты.Вставить("СуммаДолга");
Реквизиты.Вставить("ДокументОснование");
Реквизиты.Вставить("Ответственный");
Реквизиты.Вставить("Комментарий");
Реквизиты.Вставить("НомерКвитанции");
Реквизиты.Вставить("ВидКодУслуги");
Реквизиты.Вставить("ПризнакОплаты");
Реквизиты.Вставить("ВыгруженаВСРМ");
Реквизиты.Вставить("ВыгруженаНаШлюз");
Реквизиты.Вставить("ВыгруженаНаСайт");
Реквизиты.Вставить("КрайнийСрокОплаты");
Реквизиты.Вставить("ВидКвитанции");
Реквизиты.Вставить("НомерЗаказа");
Реквизиты.Вставить("ПроизвольныйАдрес");
Реквизиты.Вставить("GUID");
Реквизиты.Вставить("Автор");
Реквизиты.Вставить("ДатаИзменения");
Реквизиты.Вставить("ДатаОкончанияСтрахования", "ДокументОснование.ДатаОкончанияСтрахования");
Реквизиты.Вставить("ВидКодУслугиНаименованиеУЖКХ", "ВидКодУслуги.НаименованиеУЖКХ");

// Табличные части
Реквизиты.Вставить("Товары");
Реквизиты.Вставить("Услуги");

Данные = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ОбъектОбработки.Ссылка, Реквизиты);

Если Не Данные.Проведен И Не Данные.ПометкаУдаления Тогда
	СостояниеСообщения = ПредопределенноеЗначение("Перечисление.сшпСтатусыСообщений.ОтправкаПодтверждена"); 
	Перейти ~Выход;
КонецЕсли;

НаОснованииДоговораСтрахования = ТипЗнч(Данные.ДокументОснование) = Тип("ДокументСсылка.АБС_ДоговорСтрахования");

////////////////////////////////////////////////////////////////////////////////
// Подготовка данных

// Обработка ссылочных реквизитов
СсылочныеРеквизиты = Новый Массив;
СсылочныеРеквизиты.Добавить("Организация"); // Справочник.Организации
СсылочныеРеквизиты.Добавить("Проект"); // Справочник.Проекты
СсылочныеРеквизиты.Добавить("ЛицевойСчет"); // Справочник.КВП_ЛицевыеСчета
СсылочныеРеквизиты.Добавить("Склад"); // Справочник.Склады
СсылочныеРеквизиты.Добавить("ПодразделениеОрганизации"); // Справочник.ПодразделенияОрганизаций
СсылочныеРеквизиты.Добавить("Контрагент"); // Справочник.Контрагенты
СсылочныеРеквизиты.Добавить("ДоговорКонтрагента"); // Справочник.ДоговорыКонтрагентов
СсылочныеРеквизиты.Добавить("ОрганизацияПолучатель"); // Справочник.Организации
СсылочныеРеквизиты.Добавить("СтруктурнаяЕдиница"); // Справочник.БанковскиеСчета
СсылочныеРеквизиты.Добавить("ВалютаДокумента"); // Справочник.Валюты
СсылочныеРеквизиты.Добавить("ТипЦен"); // Справочник.ТипыЦенНоменклатуры
СсылочныеРеквизиты.Добавить("ДокументОснование"); // Документ.АБС_ДоговорСтрахования, Документ.АБС_ДоговорОформленияПравСобственности, Документ.АБС_Продажа, Документ.АБС_ДоговорОценки, Документ.АБС_ДоговорПерепланировки, Документ.АБС_ДоговорСопровожденияРемонта, Документ.АБС_РеализацияПрочихПлатныхУслуг, Документ.АБС_ДоговорНакопленияКГМ
СсылочныеРеквизиты.Добавить("Ответственный"); // Справочник.Пользователи
СсылочныеРеквизиты.Добавить("ВидКодУслуги"); // Справочник.АБС_КодыУслуг
СсылочныеРеквизиты.Добавить("ПризнакОплаты");  
СсылочныеРеквизиты.Добавить("ВидКвитанции");  
СсылочныеРеквизиты.Добавить("Автор"); // Справочник.Пользователи

Для каждого Реквизит Из СсылочныеРеквизиты Цикл
	Данные.Вставить(Реквизит, сшпОбщегоНазначения.ВыполнитьФункцию("СтруктураЗначенияСсылки", Данные[Реквизит]));
КонецЦикла;

// Обработка табличных частей
ИмяТЧ = "Товары";
Данные.Вставить(ИмяТЧ, ОбщегоНазначения.ТаблицаЗначенийВМассив(Данные[ИмяТЧ].Выгрузить()));
Для Каждого текСтрока Из Данные[ИмяТЧ] Цикл
	текСтрока.Вставить("Номенклатура", сшпОбщегоНазначения.ВыполнитьФункцию("СтруктураЗначенияСсылки", текСтрока.Номенклатура)); // Справочник.Номенклатура
	текСтрока.Вставить("СтавкаНДС", сшпОбщегоНазначения.ВыполнитьФункцию("СтруктураЗначенияСсылки", текСтрока.СтавкаНДС));  
КонецЦикла;

ИмяТЧ = "Услуги";
Данные.Вставить(ИмяТЧ, ОбщегоНазначения.ТаблицаЗначенийВМассив(Данные[ИмяТЧ].Выгрузить()));
Для Каждого текСтрока Из Данные[ИмяТЧ] Цикл
	текСтрока.Вставить("Номенклатура", сшпОбщегоНазначения.ВыполнитьФункцию("СтруктураЗначенияСсылки", текСтрока.Номенклатура)); // Справочник.Номенклатура
	текСтрока.Вставить("СтавкаНДС", сшпОбщегоНазначения.ВыполнитьФункцию("СтруктураЗначенияСсылки", текСтрока.СтавкаНДС));  
КонецЦикла;

////////////////////////////////////////////////////////////////////////////////
// Дополнительное заполнение реквизитов

ПутьКФайлу = сшпОбщегоНазначения.ВыполнитьФункцию("ЖКХ.ПутьКФайлуСчетаКвитанцииНаОплатуПокупателю", Данные.Ссылка);

Если НаОснованииДоговораСтрахования = Ложь Тогда
	
	Если Данные.ВидКодУслуги.ИмяПредопределенныхДанных <> "СтрахованиеВЕПД" Тогда //Пока все лишние отсекаем
		СостояниеСообщения = ПредопределенноеЗначение("Перечисление.сшпСтатусыСообщений.ОтправкаПодтверждена"); 
	    Перейти ~Выход;
	КонецЕсли;	
	
	Если ПустаяСтрока(ПутьКФайлу) Тогда 
		СостояниеСообщения = ПредопределенноеЗначение("Перечисление.сшпСтатусыСообщений.ОтправкаПодтверждена"); 
	    Перейти ~Выход;
	КонецЕсли;
	
	Данные.Вставить("Файл", Base64Строка(Новый ДвоичныеДанные(ПутьКФайлу))); 
	
Иначе

	Если Не ПустаяСтрока(ПутьКФайлу) Тогда 
		
		Попытка //файла физически может не быть
			Данные.Вставить("Файл", Base64Строка(Новый ДвоичныеДанные(ПутьКФайлу))); 
		Исключение
		КонецПопытки;

	КонецЕсли;
	
КонецЕсли;

Запрос = Новый Запрос;
Запрос.Текст = 
		"ВЫБРАТЬ
		|	АБС_ДенежныеСредстваВПути.Ссылка КАК Ссылка
		|ИЗ
		|	Документ.АБС_ДенежныеСредстваВПути КАК АБС_ДенежныеСредстваВПути
		|ГДЕ
		|	АБС_ДенежныеСредстваВПути.Проведен
		|	И АБС_ДенежныеСредстваВПути.ДокументОснование = &ДокументОснование";
	
Запрос.УстановитьПараметр("ДокументОснование", Данные.Ссылка);
	
РезультатЗапроса = Запрос.Выполнить();

Данные.Вставить("Оплачено", Не РезультатЗапроса.Пустой());

Properties = Новый Структура;

ЛицевойСчет = Справочники.КВП_ЛицевыеСчета.ПолучитьСсылку(Новый УникальныйИдентификатор(Данные.ЛицевойСчет.Значение));
Properties.Вставить("to_uzkh", ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ЛицевойСчет, "ЗарегистрированВУЖКХ"));

РезультатОбработки.Properties = Properties;

//crm_ОбщегоНазначенияКлиентСервер.СообщитьПользователю(СостояниеСообщения);

////////////////////////////////////////////////////////////////////////////////
// Отправка данных
РезультатОбработки.Body = сшпОбщегоНазначения.ПреобразоватьСтруктуруПоФормату(ФорматСообщения, Данные);



РезультатОбработки.ClassId = 108;

	Реквизиты = Новый Структура;
	Реквизиты.Вставить("Ссылка");
	Реквизиты.Вставить("ПометкаУдаления");
	Реквизиты.Вставить("Владелец");
	Реквизиты.Вставить("Родитель");
	Реквизиты.Вставить("Наименование");
	Реквизиты.Вставить("Код");
	Реквизиты.Вставить("КоличествоКомнат");
	Реквизиты.Вставить("GUIDУПН");
	Реквизиты.Вставить("Подъезд");
	Реквизиты.Вставить("КадастровыйНомер");
	Реквизиты.Вставить("ВидПомещения");
	Реквизиты.Вставить("АБС_ВыгружатьВбазис", "Владелец.АБС_ВыгружатьВбазис");
	Реквизиты.Вставить("Этаж");
	Реквизиты.Вставить("ПодъездНаименование", "Подъезд.Наименование");
	Реквизиты.Вставить("Характеристика");
	Реквизиты.Вставить("ПодъездВладелец", "Подъезд.Владелец");

	Данные = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ОбъектОбработки.Ссылка, Реквизиты);

	////////////////////////////////////////////////////////////////////////////////
	// Подготовка данных

	Данные.Вставить("КоличествоКомнат", Данные.КоличествоКомнат);
	Данные.Вставить("ОбщаяПлощадь", АБС_Базис.ПолучитьОбщуюПлощадьФакт(Данные.Ссылка));
	Данные.Вставить("ЖилаяПлощадь", АБС_Базис.ПолучитьЖилуюПлощадьФакт(Данные.Ссылка));
	Данные.Вставить("bazis_id", АБС_Базис.ИД_Базис(Данные.Ссылка));
	Данные.Вставить("house_id", АБС_Базис.ИД_Базис(Данные.Владелец));
	Данные.Вставить("floor_id", АБС_Базис.ИД_Базис(АБС_Базис.ЭтажПомещения(Данные.Ссылка))); 
	Данные.Вставить("section_id", АБС_Базис.ИД_Базис(Данные.Подъезд));
	Данные.Вставить("room_type_id", АБС_Базис.ОпределениеТипаПомещения(Данные.Ссылка));
	Если Не ЗначениеЗаполнено(Данные.bazis_id) Тогда
		Данные.Вставить("status_id", сшпОбщегоНазначения.ВыполнитьФункцию("ЖКХ.СтатусПомещенияБазис", Данные.Ссылка)); 
	КонецЕсли;
	Данные.Вставить("ВидПомещения", сшпОбщегоНазначения.ВыполнитьФункцию("ЖКХ.ВидПомещения", Данные.ВидПомещения));	
	СвойстваСделки = сшпОбщегоНазначения.ВыполнитьФункцию("ЖКХ.СвойстваСделкиПоПомещению", Данные.Ссылка);
	Данные.Вставить("СтатусСделки", СвойстваСделки.Статус);
	Данные.Вставить("ОстатокСделки", СвойстваСделки.Остаток);	
	
	ЗданиеПомещенияИПодъездаСовпадают = Данные.Владелец = Данные.ПодъездВладелец;   

	// Обработка ссылочных реквизитов
	СсылочныеРеквизиты = Новый Массив;
	СсылочныеРеквизиты.Добавить("Владелец"); // Справочник.КВП_Здания
	СсылочныеРеквизиты.Добавить("Родитель"); // Справочник.УПЖКХ_Помещения

	Для каждого Реквизит Из СсылочныеРеквизиты Цикл
		Данные.Вставить(Реквизит, сшпОбщегоНазначения.ВыполнитьФункцию("СтруктураЗначенияСсылки", Данные[Реквизит]));
	КонецЦикла;   	

	ВыгружатьВБазис = Не Данные.ПометкаУдаления И Данные.АБС_ВыгружатьВбазис И ЗданиеПомещенияИПодъездаСовпадают И ЗначениеЗаполнено(Данные.floor_id);

	Если ВыгружатьВБазис = Истина
		И Не ЗначениеЗаполнено(Данные.bazis_id) Тогда
		
		УИ_Ссылки = Данные.Ссылка.УникальныйИдентификатор();
		
		Запись = РегистрыСведений.сшпИнтеграцияЗапрошенныеИдентификаторы.СоздатьМенеджерЗаписи();
		Запись.ИдентификаторВнешний = УИ_Ссылки;
		Запись.КлассСообщения = РезультатОбработки.ClassId;
		Запись.Узел = "Bazis";
		Запись.Прочитать();
		Если Запись.Выбран() Тогда
			ВыгружатьВБазис = Ложь;
		Иначе
			Запись.ИдентификаторВнешний = УИ_Ссылки;
			Запись.КлассСообщения = РезультатОбработки.ClassId;
			Запись.Узел = "Bazis";
			Запись.ДатаЗапроса = ТекущаяДатаСеанса();
			Запись.Записать();
		КонецЕсли; 
		
	КонецЕсли;

	Properties = Новый Структура;

	// для Базис.Ключи
	Properties.Вставить("access_token", ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ПредопределенноеЗначение("Справочник.АБС_Базис_токен.Access_token"), "Token"));
	Properties.Вставить("bazis_id", Формат(Данные.bazis_id, "ЧН=0; ЧГ=0"));
	Properties.Вставить("to_bazis", ВыгружатьВБазис);
	Properties.Вставить("GUIDУПН", Данные.GUIDУПН);

	//для УЖКХ.Ключи
	Properties.Вставить("to_uzkh", Истина);

	РезультатОбработки.Properties = Properties;

	////////////////////////////////////////////////////////////////////////////////
	// Отправка данных
	РезультатОбработки.Body = сшпОбщегоНазначения.ПреобразоватьСтруктуруПоФормату(ФорматСообщения, Данные);

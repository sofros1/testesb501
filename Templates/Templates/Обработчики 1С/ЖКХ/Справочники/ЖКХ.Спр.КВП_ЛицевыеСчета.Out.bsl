	РезультатОбработки.ClassId = 173;

	ЛицевойСчет = ОбъектОбработки.Ссылка; //Для того, чтобы не было ошибки "Поле объекта недоступно для записи (Ссылка)"
	Данные = сшпОбщегоНазначения.ВыполнитьФункцию("ЖКХ.ДанныеЛицевогоСчета", ЛицевойСчет);
	
	Properties = Новый Структура;
	Properties.Вставить("to_uzkh", Данные.ЗарегистрированВУЖКХ = Истина); // "= Истина", т.к. для группы Данные.ЗарегистрированВУЖКХ = NULL
	РезультатОбработки.Properties = Properties;
				
	РезультатОбработки.Body = сшпОбщегоНазначения.ПреобразоватьСтруктуруПоФормату(ФорматСообщения, Данные);


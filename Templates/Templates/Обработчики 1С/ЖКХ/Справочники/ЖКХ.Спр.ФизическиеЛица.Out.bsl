	РезультатОбработки.ClassId = 109;

	Реквизиты = Новый Массив;
	Реквизиты.Добавить("ИмяПредопределенныхДанных");
	Реквизиты.Добавить("Предопределенный");
	Реквизиты.Добавить("Ссылка");
	Реквизиты.Добавить("ПометкаУдаления");
	Реквизиты.Добавить("ЭтоГруппа");
	Реквизиты.Добавить("Родитель");
	Реквизиты.Добавить("Наименование");
	Реквизиты.Добавить("Код");
	Реквизиты.Добавить("ДатаРождения");
	Реквизиты.Добавить("Пол");
	Реквизиты.Добавить("ИНН");
	Реквизиты.Добавить("СтраховойНомерПФР");
	Реквизиты.Добавить("МестоРождения");
	Реквизиты.Добавить("ГруппаДоступа");
	Реквизиты.Добавить("УдалитьПол");
	Реквизиты.Добавить("ФИО");
	Реквизиты.Добавить("УточнениеНаименования");
	Реквизиты.Добавить("ДатаРегистрации");
	Реквизиты.Добавить("НаименованиеСлужебное");
	Реквизиты.Добавить("ОсновнойБанковскийСчет");
	Реквизиты.Добавить("ПостоянноПроживалВКрыму18Марта2014Года");
	Реквизиты.Добавить("Фамилия");
	Реквизиты.Добавить("Имя");
	Реквизиты.Добавить("Отчество");
	Реквизиты.Добавить("Инициалы");
	Реквизиты.Добавить("МестоРожденияПредставление");
	Реквизиты.Добавить("ЛьготаПриНачисленииПособий");
	Реквизиты.Добавить("УдалитьИнициалыИмени");
	Реквизиты.Добавить("ФаксимилеПодписи");
	Реквизиты.Добавить("GUID");
	Реквизиты.Добавить("ЗарегистрированВУЖКХ");
	// Табличные части
	Реквизиты.Добавить("КонтактнаяИнформация");
	Реквизиты.Добавить("ДополнительныеРеквизиты");

	Данные = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ОбъектОбработки.Ссылка, Реквизиты);  	

	////////////////////////////////////////////////////////////////////////////////
	// Подготовка данных

	// Обработка ссылочных реквизитов
	СсылочныеРеквизиты = Новый Массив;
	СсылочныеРеквизиты.Добавить("Родитель"); // Справочник.ФизическиеЛица
	СсылочныеРеквизиты.Добавить("Пол");  
	СсылочныеРеквизиты.Добавить("ГруппаДоступа"); // Справочник.ГруппыДоступаФизическихЛиц
	СсылочныеРеквизиты.Добавить("УдалитьПол");  
	СсылочныеРеквизиты.Добавить("ОсновнойБанковскийСчет"); // Справочник.БанковскиеСчета
	СсылочныеРеквизиты.Добавить("ЛьготаПриНачисленииПособий");  
	СсылочныеРеквизиты.Добавить("ФаксимилеПодписи"); // Справочник.ФизическиеЛицаПрисоединенныеФайлы

	Для каждого Реквизит Из СсылочныеРеквизиты Цикл
		Данные.Вставить(Реквизит, сшпОбщегоНазначения.ВыполнитьФункцию("СтруктураЗначенияСсылки", Данные[Реквизит]));
	КонецЦикла;

	// Обработка табличных частей
	ИмяТЧ = "КонтактнаяИнформация";
	Данные.Вставить(ИмяТЧ, ОбщегоНазначения.ТаблицаЗначенийВМассив(Данные[ИмяТЧ].Выгрузить()));
	Для Каждого текСтрока Из Данные[ИмяТЧ] Цикл
		текСтрока.Вставить("Тип", сшпОбщегоНазначения.ВыполнитьФункцию("СтруктураЗначенияСсылки", текСтрока.Тип));  
		текСтрока.Вставить("Вид", сшпОбщегоНазначения.ВыполнитьФункцию("СтруктураЗначенияСсылки", текСтрока.Вид)); // Справочник.ВидыКонтактнойИнформации
		текСтрока.Вставить("ВидДляСписка", сшпОбщегоНазначения.ВыполнитьФункцию("СтруктураЗначенияСсылки", текСтрока.ВидДляСписка)); // Справочник.ВидыКонтактнойИнформации
	КонецЦикла;

	ИмяТЧ = "ДополнительныеРеквизиты";
	Данные.Вставить(ИмяТЧ, ОбщегоНазначения.ТаблицаЗначенийВМассив(Данные[ИмяТЧ].Выгрузить()));
	Для Каждого текСтрока Из Данные[ИмяТЧ] Цикл
		текСтрока.Вставить("Свойство", сшпОбщегоНазначения.ВыполнитьФункцию("СтруктураЗначенияСсылки", текСтрока.Свойство)); // ПланВидовХарактеристик.ДополнительныеРеквизитыИСведения
		текСтрока.Вставить("Значение", сшпОбщегоНазначения.ВыполнитьФункцию("СтруктураЗначенияСсылки", текСтрока.Значение)); 
	КонецЦикла;

	// Заполним ФИО актуальными значениями
	ФИО = сшпОбщегоНазначения.ВыполнитьФункцию("БП.ФИОФизическихЛиц", Данные.Ссылка);
	ЗаполнитьЗначенияСвойств(Данные, ФИО);
	
	////////////////////////////////////////////////////////////////////////////////
	// Заполнение реквизитов для УЖКХ
	
	//++ @Бессчетнов.Ярослав 05.10.2022 UKKC3-60  
	Ссылка = ОбъектОбработки.Ссылка; //Для того, чтобы не было ошибки "Поле объекта недоступно для записи"
	МенеджерОбъекта = Справочники.crm_АБС_Клиенты; //Для того, чтобы не было ошибки "Поле объекта недоступно для записи"
	Клиент = сшпОбщегоНазначения.ВыполнитьФункцию("ЖКХ.НайтиОбъектCRMПоОбъектуЖКХ", Ссылка, МенеджерОбъекта);
		
	Если ЗначениеЗаполнено(Клиент) Тогда 
		РеквизитыКлиента = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Клиент, "ТелефонУЖКХ, АдресЭлектроннойПочтыУЖКХ, ДатаРождения"); 
		Данные.Вставить("ТелефонУЖКХ", РеквизитыКлиента.ТелефонУЖКХ);
		Данные.Вставить("АдресЭлектроннойПочтыУЖКХ", РеквизитыКлиента.АдресЭлектроннойПочтыУЖКХ);
		Данные.Вставить("ДатаРождения", РеквизитыКлиента.ДатаРождения);
	Иначе
		//Замечено, что если создается новое физ. лицо, клиента еще нет
		//ВызватьИсключение "Не найден элемент справочника crm_АБС_Клиенты по физ. лицу";  		
	КонецЕсли;	
	//-- @Бессчетнов.Ярослав 05.10.2022

	////////////////////////////////////////////////////////////////////////////////
	// Заполнение реквизитов для Базис

	//ВыгружатьВБазис = АБС_Базис.ТранслироватьПользователя(Данные.Ссылка);
	Запрос = Новый Запрос(сшпОбщегоНазначения.ВыполнитьФункцию("ЖКХ.ТекстЗапроса_ФизическиеЛицаКВыгрузкеВБазис", Данные.Ссылка));
	Запрос.УстановитьПараметр("Клиент", Данные.Ссылка);
	РезультатЗапроса = Запрос.Выполнить();
	ВыгружатьВБазис = Не РезультатЗапроса.Пустой();

	Контакты = АБС_Базис.КонтактыФизическогоЛица(Данные.Ссылка);

	//++ @Бессчетнов.Ярослав 15.09.2023 
	//ТелефоныКлиентов = сшпОбщегоНазначения.ВыполнитьФункцию("ЖКХ.ТелефоныКлиентов", Данные.Ссылка);
	//НомерТелефона = ТелефоныКлиентов.Получить(Данные.Ссылка);
	//НомерТелефона = АБС_Базис.ТолькоЦифры(СокрЛП(НомерТелефона));

	//Если СтрДлина(НомерТелефона) <> 11 Тогда
	//	ВыгружатьВБазис = Ложь;				
	//КонецЕсли;                       
	
	ОписаниеОшибки = "";
	НомерТелефона = сшпОбщегоНазначения.ВыполнитьФункцию("ЖКХ.НомерТелефонаКлиента", Клиент, ОписаниеОшибки);  
	
	Если ПустаяСтрока(ОписаниеОшибки) Тогда 
			
		НомерТелефона = АБС_Базис.ТолькоЦифры(СокрЛП(НомерТелефона));
			
		Если СтрДлина(НомерТелефона) <> 11 Тогда 
			ОписаниеОшибки = СтрШаблон("В номере телефона должно быть 11 цифр '%1'", НомерТелефона);
		КонецЕсли;
			
	КонецЕсли;
		
	Если Не ПустаяСтрока(ОписаниеОшибки) Тогда 
		
		ВыгружатьВБазис = Ложь;
		
		//ЗаписьЖурналаРегистрации("Bazis. Ошибка выгрузки данных в Базис", УровеньЖурналаРегистрации.Ошибка, Данные.Ссылка.Метаданные(), Данные.Ссылка, 
		//	СтрШаблон("Не удалось выгрузить объект %1 в Базис.  
		//	|Обработчик: ЖКХ.Спр.ФизическиеЛица.Out
		//	|Сообщение: %2", Данные.Ссылка, ОписаниеОшибки));
			
	КонецЕсли;
	
	//-- @Бессчетнов.Ярослав 15.09.2023

	Данные.Вставить("bazis_id",  АБС_Базис.ИД_Базис(Данные.Ссылка));
	Данные.Вставить("user_group_id", 10); //клиент
	Данные.Вставить("phone", НомерТелефона);
	Данные.Вставить("email", Контакты.Почта);
	Данные.Вставить("password",  "123456789");   
	
	//++ @Бессчетнов.Ярослав 13.07.2023 [BZZ-23]  
	Если ВыгружатьВБазис Тогда
	
		ПаспортныеДанные = Новый Структура("Серия, Номер, ДатаВыдачи, КемВыдан, КодПодразделения"); 
		ДокументыФизЛица = АБС_Базис.ДокументыФизЛица(Данные.Ссылка); 
		Если ЗначениеЗаполнено(ДокументыФизЛица) Тогда 
			ЗаполнитьЗначенияСвойств(ПаспортныеДанные, ДокументыФизЛица);   
		КонецЕсли;  
		Данные.Вставить("ПаспортныеДанные", ПаспортныеДанные);		
	
		Адреса = сшпОбщегоНазначения.ВыполнитьФункцию("ЖКХ.АдресаФизическогоЛица", Данные.Ссылка);
		Данные.Вставить("АдресРегистрации", Адреса.АдресПрописки);
		Данные.Вставить("АдресПроживания", ?(ЗначениеЗаполнено(Адреса.ФактАдресКонтрагента), Адреса.ФактАдресКонтрагента, Адреса.АдресПрописки));
		
	КонецЕсли;
	//-- @Бессчетнов.Ярослав 13.07.2023

	Если ВыгружатьВБазис = Истина
		И Не ЗначениеЗаполнено(Данные.bazis_id) Тогда
		
		Запись = РегистрыСведений.сшпИнтеграцияЗапрошенныеИдентификаторы.СоздатьМенеджерЗаписи();
		Запись.ИдентификаторВнешний = Данные.Ссылка.УникальныйИдентификатор();
		Запись.КлассСообщения = РезультатОбработки.ClassId;
		Запись.Узел = "Bazis";
		Запись.Прочитать();
		Если Запись.Выбран() Тогда
			ВыгружатьВБазис = Ложь;
		Иначе
			Запись.ИдентификаторВнешний = Данные.Ссылка.УникальныйИдентификатор();
			Запись.КлассСообщения = РезультатОбработки.ClassId;
			Запись.Узел = "Bazis";
			Запись.ДатаЗапроса = ТекущаяДатаСеанса();;
			Запись.Записать();
		КонецЕсли; 
		
	КонецЕсли;   
	
	Properties = Новый Структура;

	// для Базис.Ключи
	Properties.Вставить("access_token", СокрЛП(Справочники.АБС_Базис_токен.Access_token.Token));
	Properties.Вставить("bazis_id", Формат(Данные.bazis_id, "ЧН=0; ЧГ=0"));
	Properties.Вставить("to_bazis", ВыгружатьВБазис = Истина);
	//++ @Бессчетнов.Ярослав 28.07.2023 [BZZ-33] 
	Properties.Вставить("GUID", Данные.GUID); //Возможно придется переделать на Контрагент.GUID
	//-- @Бессчетнов.Ярослав 28.07.2023	

	//для УЖКХ.Ключи
	Properties.Вставить("to_uzkh", Данные.ЗарегистрированВУЖКХ = Истина);  
	
	////////////////////////////////////////////////////////////////////////////////
	// Отправка данных
	РезультатОбработки.Properties = Properties;

	РезультатОбработки.Body = сшпОбщегоНазначения.ПреобразоватьСтруктуруПоФормату(ФорматСообщения, Данные);

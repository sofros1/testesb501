РезультатОбработки.ClassId = 13;

Ссылка = ОбъектОбработки.Ссылка;

Реквизиты = Новый Структура;
Реквизиты.Вставить("Ссылка");
Реквизиты.Вставить("ПометкаУдаления");
Реквизиты.Вставить("Наименование");

Реквизиты.Вставить("ТипЗначения");

Реквизиты.Вставить("ИмяПредопределенныхДанных");
Реквизиты.Вставить("ИдентификаторДляФормул");

Реквизиты.Вставить("Имя");

Реквизиты.Вставить("БазоваяЕдиницаИзмерения");

Данные = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Ссылка, Реквизиты);

////////////////////////////////////////////////////////////////////////////////
// Подготовка данных

Данные.Вставить("ТипЗначения", Строка(Данные.ТипЗначения));

Данные.Вставить("ВладелецСвойства", сшпОбщегоНазначения.ВыполнитьФункцию("ЕИС.ПВХ.ДополнительныеРеквизитыИСведения.ВладелецСвойства", Ссылка));

////////////////////////////////////////////////////////////////////////////////
// Свойства сообщения

РезультатОбработки.Properties = Новый Структура;

// Добавим токен

Токен = Справочники.Токены.НайтиПоНаименованию("MyABS", Истина);
Если Токен.Пустая() Тогда
	ВызватьИсключение "Не найден элемент справочника Токены по наименованию ""MyABS""";
КонецЕсли;

РезультатОбработки.Properties.Вставить("access_token", Токен.accessToken);
РезультатОбработки.Properties.Вставить("to_myabs", Истина);

////////////////////////////////////////////////////////////////////////////////
// Отправка данных
РезультатОбработки.Body = сшпОбщегоНазначения.ПреобразоватьСтруктуруПоФормату(ФорматСообщения, Данные);

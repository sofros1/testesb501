РезультатОбработки.ClassId = 11;

Ссылка = ОбъектОбработки.Ссылка;

Реквизиты = Новый Структура;
Реквизиты.Вставить("Ссылка");
Реквизиты.Вставить("Код");
Реквизиты.Вставить("Наименование");
Реквизиты.Вставить("ЭтоГруппа");
Реквизиты.Вставить("ПометкаУдаления");
Реквизиты.Вставить("Родитель");
Реквизиты.Вставить("GUID");
Реквизиты.Вставить("НаименованиеПолное");
Реквизиты.Вставить("ВидМатериала");
Реквизиты.Вставить("ЕдиницаИзмерения");
Реквизиты.Вставить("Номинированный");

Данные = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Ссылка, Реквизиты);

Набор = РегистрыСведений.ДополнительныеСведения.СоздатьНаборЗаписей();
Набор.Отбор.Найти("Объект").Установить(Ссылка);
Набор.Прочитать();
Набор = Набор.Выгрузить();
Набор.Колонки.Удалить("Объект");

Данные.Вставить("ДополнительныеСведения", ОбщегоНазначения.ТаблицаЗначенийВМассив(Набор));

////////////////////////////////////////////////////////////////////////////////
// Подготовка данных

Данные.Вставить("Код", ?(ПустаяСтрока(Данные.Код), "000000000", Данные.Код));

Если Данные.Родитель.Пустая() Тогда
	Данные.Вставить("Родитель", Неопределено);
КонецЕсли;
Если Данные.ВидМатериала.Пустая() Тогда
	Данные.Вставить("ВидМатериала", Неопределено);
КонецЕсли;

Данные.Вставить("ЕдиницаИзмеренияКодПоОКЕИ", "");
Если Данные.ЕдиницаИзмерения.Пустая() Тогда
	Данные.Вставить("ЕдиницаИзмерения", Неопределено);
Иначе
	ДанныеЕдиницыИзмерения = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Данные.ЕдиницаИзмерения, Новый Структура("Код,ИзОКЕИ"));
	Если ДанныеЕдиницыИзмерения.ИзОКЕИ Тогда
		Данные.Вставить("ЕдиницаИзмеренияКодПоОКЕИ", ДанныеЕдиницыИзмерения.Код);
	КонецЕсли;
КонецЕсли;	
	
// Добавим токен

Токен = Справочники.Токены.НайтиПоНаименованию("MyABS", Истина);
Если Токен.Пустая() Тогда
	ВызватьИсключение "Не найден элемент справочника Токены по наименованию ""MyABS""";
КонецЕсли;

Properties = Новый Структура;
Properties.Вставить("access_token", Токен.accessToken);
Properties.Вставить("to_myabs", Истина);

РезультатОбработки.Properties = Properties;

////////////////////////////////////////////////////////////////////////////////
// Отправка данных
РезультатОбработки.Body = сшпОбщегоНазначения.ПреобразоватьСтруктуруПоФормату(ФорматСообщения, Данные);

РезультатОбработки.ClassId = 16;

Реквизиты = Новый Структура;
Реквизиты.Вставить("Ссылка");
Реквизиты.Вставить("ПометкаУдаления");
Реквизиты.Вставить("ЭтоГруппа");
Реквизиты.Вставить("Родитель");
Реквизиты.Вставить("Наименование");
Реквизиты.Вставить("Код");
Реквизиты.Вставить("ПеременныеФормулыСтрока");
Реквизиты.Вставить("Расширяемый");
Реквизиты.Вставить("GUID");
Реквизиты.Вставить("НаименованиеПолное");
Реквизиты.Вставить("Работы");
Реквизиты.Вставить("ВидыМатериалов");
Реквизиты.Вставить("СписокПараметровBIM");
Реквизиты.Вставить("Классификатор");  // Нику

Данные = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ОбъектОбработки.Ссылка, Реквизиты);

////////////////////////////////////////////////////////////////////////////////
// Подготовка данных

Данные.Вставить("Код", ?(ПустаяСтрока(Данные.Код), "000000000", Данные.Код));
// Нику -->
//Данные.Вставить("КлассификаторВидовКонструкций", РегистрыСведений.бим_СоответствияКлассификаторТехнология.Получить(Новый Структура("Технология", ОбъектОбработки.Ссылка)).Классификатор);
Данные.Вставить("КлассификаторВидовКонструкций", ?(Не ЗначениеЗаполнено(Данные.Классификатор), null, Данные.Классификатор)); 
Данные.Удалить("Классификатор"); 
// <-- Нику

Если Данные.Родитель.Пустая() Тогда
	Данные.Вставить("Родитель", Неопределено);
КонецЕсли;

Данные.Вставить("Толщина", 0);

Данные.Вставить("Работы", ОбщегоНазначения.ТаблицаЗначенийВМассив(Данные.Работы.Выгрузить()));
Данные.Вставить("ВидыМатериалов", ОбщегоНазначения.ТаблицаЗначенийВМассив(Данные.ВидыМатериалов.Выгрузить()));

ТаблицаПФ = Данные.СписокПараметровBIM.Выгрузить();
МассивПФ = ТаблицаПФ.ВыгрузитьКолонку("ПеременнаяФормулы");

Для Каждого текСтрока Из Данные.Работы Цикл 
		
		текСтрока.Вставить("Норма", Окр(текСтрока.Норма, 3));
		текСтрока.Вставить("ФормулаДляMyABS", текСтрока.Формула);
		
		МассивПФ =  сшпОбщегоНазначения.ВыполнитьФункцию("ЕИС.BIM.МассивПеременныхФормулИзФормул", текСтрока.ФормулаДляMyABS, МассивПФ);
		
КонецЦикла; 

Для Каждого текСтрока Из Данные.ВидыМатериалов Цикл 
		
		текСтрока.Вставить("Норма", Окр(текСтрока.Норма, 3));
		
		Инд = 0;
		Если Не текСтрока.ФункцияМатериала.Пустая() Тогда
			Инд = Перечисления.бим_ФункцииМатериалов.Индекс(текСтрока.ФункцияМатериала) + 1;
		КонецЕсли;
		текСтрока.Вставить("ФункцияМатериалаСтрока", СокрЛП( текСтрока.ФункцияМатериала));
		текСтрока.Вставить("ФункцияМатериала", Формат(Инд, "ЧН=0; ЧГ=0"));

		текСтрока.Вставить("ФормулаДляMyABS", текСтрока.Формула);
		
		МассивПФ =  сшпОбщегоНазначения.ВыполнитьФункцию("ЕИС.BIM.МассивПеременныхФормулИзФормул", текСтрока.ФормулаДляMyABS, МассивПФ);
		
		Данные.Толщина = Данные.Толщина + текСтрока.Толщина;
КонецЦикла; 

Отбор = Новый Структура;
Для каждого ПФ Из МассивПФ Цикл
	
	Отбор.Вставить("ПеременнаяФормулы", ПФ);
	Если ТаблицаПФ.НайтиСтроки(Отбор).Количество() = 0 Тогда
		ТаблицаПФ.Добавить().ПеременнаяФормулы = ПФ;
	КонецЕсли;
	
КонецЦикла;

Данные.Вставить("СписокПараметровBIM", Новый Массив);

НомерСтроки = 0;
Для Каждого СтрокаПФ Из ТаблицаПФ Цикл
	
	НомерСтроки = НомерСтроки + 1;
	
	СтрокаТЧ = Новый Структура;
	СтрокаТЧ.Вставить("Ссылка", Данные.Ссылка);
	СтрокаТЧ.Вставить("НомерСтроки", НомерСтроки);
	СтрокаТЧ.Вставить("ПеременнаяФормулы", СтрокаПФ.ПеременнаяФормулы);
	СтрокаТЧ.Вставить("Комментарий", СтрокаПФ.Комментарий);
	СтрокаТЧ.Вставить("ЕдиницаИзмерения", 		СтрокаПФ.ЕдиницаИзмерения);
	
	Данные.СписокПараметровBIM.Добавить(СтрокаТЧ);

КонецЦикла;

// Добавим токен

Токен = Справочники.Токены.НайтиПоНаименованию("MyABS", Истина);
Если Токен.Пустая() Тогда
	ВызватьИсключение "Не найден элемент справочника Токены по наименованию ""MyABS""";
КонецЕсли;

Properties = Новый Структура;
Properties.Вставить("access_token", Токен.accessToken);
Properties.Вставить("to_myabs", Истина);

РезультатОбработки.Properties = Properties;

////////////////////////////////////////////////////////////////////////////////
// Отправка данных
РезультатОбработки.Body = сшпОбщегоНазначения.ПреобразоватьСтруктуруПоФормату(ФорматСообщения, Данные);

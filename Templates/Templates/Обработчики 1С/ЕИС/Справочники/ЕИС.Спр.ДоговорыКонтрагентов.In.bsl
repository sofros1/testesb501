//Определение переменных
ПустойИдентификатор = "00000000-0000-0000-0000-000000000000";

//Получение тела сообщения
xdtoОбъект = сшпОбщегоНазначения.ПолучитьОбъектXDTO(ФорматСообщения, ОбъектСообщение.Body);

БазаИсточник = ОбъектСообщение.Source;

ИсточникЕСЭД = Лев(БазаИсточник, 4) = "CDWF"; 
Если ИсточникЕСЭД Тогда

	НаборРС = РегистрыСведений.ОбъектыИнтегрированныеС1СДокументооборотом.СоздатьНаборЗаписей();
	НаборРС.Отбор.ТипОбъектаДокументооборота.Установить("DMInternalDocument");
	НаборРС.Отбор.ИдентификаторОбъектаДокументооборота.Установить(xdtoОбъект.Ссылка);
	//По ИдентификаторОбъектаДокументооборота отбор не устанавливаем, должна быть максимум 1 запись на объект ЕИСа
	
	НаборРС.Прочитать();
		
	Если НаборРС.Количество() Тогда
	
		Перейти ~Выход; // Не выгружаем повторно из ЕСЭД 
	
	КонецЕсли;	
	 
Иначе

	Перейти ~Выход; //Пока выключим выгрузку из других источников
	
КонецЕсли;	

//Поиск объекта
ЗначениеДляПоиска = Новый УникальныйИдентификатор(xdtoОбъект.Ссылка);
СсылкаНаЭлемент = Справочники.ДоговорыКонтрагентов.ПолучитьСсылку(ЗначениеДляПоиска);
новыйОбъект = СсылкаНаЭлемент.ПолучитьОбъект();

Если новыйОбъект = Неопределено тогда
		
		новыйОбъект = Справочники.ДоговорыКонтрагентов.СоздатьЭлемент();
		новыйОбъект.УстановитьСсылкуНового(СсылкаНаЭлемент);

Иначе		
	Перейти ~Выход; //Пока не выгружаем повторно
КонецЕсли;

////////////////////////////////////////////////////////////////////////////////
//Реквизиты шапки

//бит_ТипДоговора  По умолчанию = ДопРаботы
Если НЕ ЗначениеЗаполнено(новыйОбъект.бит_ТипДоговора) Тогда
	новыйОбъект.бит_ТипДоговора = Справочники.бит_ТипыДоговоров.ДопРаботы;
КонецЕсли;	

//Владелец ИЩЕМ ПО GUID
Значение = xdtoОбъект.Получить("Контрагент_GUID");
Если Значение <> ПустойИдентификатор И ЗначениеЗаполнено(Значение) Тогда
    новыйОбъект.Владелец = Справочники.Контрагенты.НайтиПоРеквизиту("GUID", Значение);
Иначе
    новыйОбъект.Владелец = Справочники.Контрагенты.ПустаяСсылка();
КонецЕсли;

//Наименование
новыйОбъект.Наименование = xdtoОбъект.Получить("Наименование");
новыйОбъект.абс_НаименованиеДоговора = xdtoОбъект.Получить("Наименование");

//GUID
новыйОбъект.GUID = xdtoОбъект.Ссылка;

//Организация ИЩЕМ ПО GUID
Значение = xdtoОбъект.Получить("Организация_GUID");
Если Значение <> ПустойИдентификатор И ЗначениеЗаполнено(Значение) Тогда
    новыйОбъект.Организация = Справочники.Организации.НайтиПоРеквизиту("GUID", Значение);
Иначе
    новыйОбъект.Организация = Справочники.Организации.ПустаяСсылка();
КонецЕсли;

//Номер
новыйОбъект.Номер = xdtoОбъект.Получить("Номер");

//СуммаДоговора
новыйОбъект.бит_СуммаДоговора = xdtoОбъект.Получить("СуммаДоговора");

//ВалютаВзаиморасчетов
ВалютаКод = xdtoОбъект.Получить("ВалютаКод");
Если ЗначениеЗаполнено(ВалютаКод) Тогда
	Валюта = Справочники.Валюты.НайтиПоКоду(ВалютаКод, Истина);
	новыйОбъект.ВалютаВзаиморасчетов = Валюта;
КонецЕсли;	

//СрокДействия
новыйОбъект.СрокДействия = ?(ЗначениеЗаполнено(xdtoОбъект.Получить("СрокДействия")), XMLЗначение(Тип("Дата"), xdtoОбъект.Получить("СрокДействия")), Дата(1,1,1));

//Дата
новыйОбъект.Дата = ?(ЗначениеЗаполнено(xdtoОбъект.Получить("Дата")), XMLЗначение(Тип("Дата"), xdtoОбъект.Получить("Дата")), Дата(1,1,1));

//Реквизиты по умолчанию
новыйОбъект.ВидДоговора = Перечисления.ВидыДоговоровКонтрагентов.СПокупателем;
новыйОбъект.ндк_ВидДДС = Перечисления.ндк_ВидыДДС.БезНал;
новыйОбъект.ндк_ВидДокумента = Перечисления.ндк_ВидДокументаПоДоговору.ПроектныйБух;

//ндк_СсылкаНаDocsVision
новыйОбъект.ндк_СсылкаНаDocsVision = xdtoОбъект.Получить("ндк_СсылкаНаDocsVision");

//СсылкаНаТендер
новыйОбъект.ндк_СсылкаНаТендер = xdtoОбъект.Получить("СсылкаНаТендер");

//Запись объекта
новыйОбъект.ДополнительныеСвойства.Вставить("СШПНеобрабатывать", Истина);
новыйОбъект.ОбменДанными.Загрузка = Истина;
новыйОбъект.Записать();

Если ИсточникЕСЭД И НаборРС.Количество() = 0 Тогда

	НоваяЗапись =  РегистрыСведений.ОбъектыИнтегрированныеС1СДокументооборотом.СоздатьМенеджерЗаписи();
	НоваяЗапись.Объект = новыйОбъект.Ссылка;
	НоваяЗапись.ТипОбъектаДокументооборота = "DMInternalDocument";
	НоваяЗапись.ИдентификаторОбъектаДокументооборота = xdtoОбъект.Ссылка;
	Если ЗначениеЗаполнено(xdtoОбъект.IDШаблонаДокумента) Тогда
		НоваяЗапись.абс_ПравилаИнтеграции  = Справочники.ПравилаИнтеграцииС1СДокументооборотом.НайтиПоРеквизиту("ШаблонID", СокрЛП(xdtoОбъект.IDШаблонаДокумента));
	КонецЕсли;
	
	НоваяЗапись.Записать();
	
	СсылкаНаОбъект = новыйОбъект.Ссылка;
	СсылкаНаДокументУчетнойСистемы = ВыполнитьФункцию("ВнешняяНавигационнаяСсылка", СсылкаНаОбъект);
	
	НаборДляЕСЭД = Новый Структура;
	НаборДляЕСЭД.Вставить("ИДВнешнегоОбъекта", Строка(новыйОбъект.Ссылка.УникальныйИдентификатор()));
	НаборДляЕСЭД.Вставить("ТипВнешнегоОбъекта", новыйОбъект.Ссылка.Метаданные().ПолноеИмя());
	НаборДляЕСЭД.Вставить("ИДОбъектаДокументооборот", xdtoОбъект.Ссылка);
	НаборДляЕСЭД.Вставить("СсылкаНаДокументУчетнойСистемы", СсылкаНаДокументУчетнойСистемы);
	
	сшпПользовательскиеМетоды.ПоместитьВОчередьИсходящих("Принудительно.РС.СвязиОбъектовИнтегрированныхСистем", НаборДляЕСЭД);
	
//	МетодХранения = ПредопределенноеЗначение("Перечисление.сшпМетодХранения.Сериализация");
//	ФорматСериализация = сшпФункциональныеОпции.ФорматСериализации();
//	ЗначениеХранения = сшпОбщегоНазначения.СериализоватьОбъект(ФорматСериализация, НаборДляЕСЭД);

//	ТипОбъекта = "Принудительно.РС.СвязиОбъектовИнтегрированныхСистем";
//	ПомещатьВХранилищеЗначения = Истина;
//	ЭтоУдаление = Ложь;
//	СсылкаНаОбъект = Неопределено;

//	Если Не ЗначениеХранения = Неопределено Тогда
//		сшпРаботаСДанными.ПоместитьВОчередьИсходящих(
//			ТипОбъекта,
//			ЗначениеХранения,
//			ФорматСериализация,
//			МетодХранения,
//			ПомещатьВХранилищеЗначения,
//			ЭтоУдаление,
//			СсылкаНаОбъект);
//	КонецЕсли;

КонецЕсли;

//Получение тела сообщения
xdtoОбъект = сшпОбщегоНазначения.ПолучитьОбъектXDTO(ФорматСообщения, ОбъектСообщение.Body);

СвойстваСообщения = ОбъектСообщение.Properties;	
	
Если ТипЗнч(СвойстваСообщения) = Тип("ОбъектXDTO") Тогда
	СвойстваСообщения = сшпОбщегоНазначения.ПолучитьПараметрыСообщенияСтруктурой(ОбъектСообщение);
КонецЕсли;

Если Не ТипЗнч(СвойстваСообщения) = Тип("Структура") Тогда
	ВызватьИсключение "Некорректный формат свойств сообщения";
КонецЕсли;

Если Не СвойстваСообщения.Свойство("ЗапросДанныхГрафикСтроительства") 
		Или СвойстваСообщения.ЗапросДанныхГрафикСтроительства <> Истина Тогда 
	//Перейти ~Выход;
КонецЕсли;

ПроектУИ = Неопределено;
Если Не СвойстваСообщения.Свойство("Проект", ПроектУИ) Тогда 
	ВызватьИсключение "Не найдено свойство сообщения ""Проект""";
КонецЕсли;

ПользовательУИ = Неопределено;
Если Не СвойстваСообщения.Свойство("Автор", ПользовательУИ) Тогда 
	ВызватьИсключение "Не найдено свойство сообщения ""Автор""";
КонецЕсли;

ЗагрузитьБазовыеСроки = Неопределено;
Если Не СвойстваСообщения.Свойство("ЗагрузитьБазовыеСроки", ЗагрузитьБазовыеСроки) Тогда 
	ВызватьИсключение "Не найдено свойство сообщения ""ЗагрузитьБазовыеСроки""";
КонецЕсли;

Если Не СтроковыеФункцииКлиентСервер.ЭтоУникальныйИдентификатор(ПроектУИ) Тогда
	ВызватьИсключение СтрШаблон("Некорретный формат уникального идентификатора проекта %1", ПроектУИ);
КонецЕсли;
ПроектСсылка = Справочники.Проекты.ПолучитьСсылку(Новый УникальныйИдентификатор(ПроектУИ));

Если Не ОбщегоНазначения.СсылкаСуществует(ПроектСсылка) Тогда 
	ВызватьИсключение СтрШаблон("Не найден элемент справочника ""Проекты"" с уникальным идентификатором %1", ПроектУИ);
КонецЕсли;

Если Не СтроковыеФункцииКлиентСервер.ЭтоУникальныйИдентификатор(ПользовательУИ) Тогда
	ВызватьИсключение СтрШаблон("Некорретный формат уникального идентификатора пользователя %1", ПользовательУИ);
КонецЕсли;
ПользовательСсылка = Справочники.Пользователи.ПолучитьСсылку(Новый УникальныйИдентификатор(ПользовательУИ));

Если Не ОбщегоНазначения.СсылкаСуществует(ПользовательСсылка) Тогда 
	ВызватьИсключение СтрШаблон("Не найден элемент справочника ""Пользователи"" с уникальным идентификатором %1", ПользовательУИ);
КонецЕсли;

СсылкаДокументаСтрокой = xdtoОбъект.Получить("Ссылка");

Если Не СтроковыеФункцииКлиентСервер.ЭтоУникальныйИдентификатор(СсылкаДокументаСтрокой) Тогда
	ВызватьИсключение СтрШаблон("Некорретный формат уникального идентификатора документа %1", СсылкаДокументаСтрокой);
КонецЕсли;
СсылкаДокумента = Документы.абс_ГрафикСтроительстваОС.ПолучитьСсылку(Новый УникальныйИдентификатор(СсылкаДокументаСтрокой));

СоздатьДокумент = Не ОбщегоНазначения.СсылкаСуществует(СсылкаДокумента);

ВнешняяТаблица = Новый ТаблицаЗначений;
ВнешняяТаблица.Колонки.Добавить("ТочкаКод", Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(9)));
ВнешняяТаблица.Колонки.Добавить("КлассификаторКодификатор", Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(36)));
ВнешняяТаблица.Колонки.Добавить("ОбъектGUID", Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(36)));
ВнешняяТаблица.Колонки.Добавить("ДатаНачала", Новый ОписаниеТипов("Дата"));
ВнешняяТаблица.Колонки.Добавить("ДатаОкончания", Новый ОписаниеТипов("Дата"));
ВнешняяТаблица.Колонки.Добавить("ДатаНачалаБазовая", Новый ОписаниеТипов("Дата"));
ВнешняяТаблица.Колонки.Добавить("ДатаОкончанияБазовая", Новый ОписаниеТипов("Дата"));

xdtoГрафик = xdtoОбъект.График.Последовательность();
Для Инд = 0 По xdtoГрафик.Количество() - 1 Цикл
	
	xdtoСтрока = xdtoГрафик.ПолучитьЗначение(Инд);
	
	НоваяСтрокаВнешнейТаблицы = ВнешняяТаблица.Добавить();
	НоваяСтрокаВнешнейТаблицы.ОбъектGUID = xdtoСтрока.Получить("Объект").Значение;
	
	Классификатор = xdtoСтрока.Получить("Классификатор");
	Если Классификатор.Тип = "CatalogRef.абс_Классификатор" Тогда
		НоваяСтрокаВнешнейТаблицы.КлассификаторКодификатор = Классификатор.Значение;
	Иначе
		НоваяСтрокаВнешнейТаблицы.ТочкаКод = Классификатор.Значение;
	КонецЕсли;
	
	Попытка 
		
		ДатаНачала = xdtoСтрока.Получить("ДатаНачала");	
		Если Не  ДатаНачала = НЕОПРЕДЕЛЕНО Тогда
			НоваяСтрокаВнешнейТаблицы.ДатаНачала = XMLЗначение(Тип("Дата"), ДатаНачала);
		КонецЕсли;
		
		ДатаОкончания = xdtoСтрока.Получить("ДатаОкончания");
		Если Не  ДатаОкончания = НЕОПРЕДЕЛЕНО Тогда
			НоваяСтрокаВнешнейТаблицы.ДатаОкончания =  XMLЗначение(Тип("Дата"), ДатаОкончания);
		КонецЕсли;
	
		Если ЗагрузитьБазовыеСроки = Истина Тогда 
			
			ДатаНачалаБазовая = xdtoСтрока.Получить("ДатаНачалаБазовая");	
			Если НЕ ДатаНачалаБазовая = НЕОПРЕДЕЛЕНО Тогда 
				НоваяСтрокаВнешнейТаблицы.ДатаНачалаБазовая =  XMLЗначение(Тип("Дата"), ДатаНачалаБазовая);
			КонецЕсли;
			
			ДатаОкончанияБазовая = xdtoСтрока.Получить("ДатаОкончанияБазовая");
			Если НЕ ДатаОкончанияБазовая  = НЕОПРЕДЕЛЕНО Тогда
				НоваяСтрокаВнешнейТаблицы.ДатаОкончанияБазовая =  XMLЗначение(Тип("Дата"), ДатаОкончанияБазовая);
			КонецЕсли;
				
		КонецЕсли;
	
	Исключение
			
			ВызватьИсключение СтрШаблон("Некорректный формат даты графика по объекту %1", НоваяСтрокаВнешнейТаблицы.ОбъектGUID);
			
	КонецПопытки;
		
КонецЦикла;

Запрос = Новый Запрос;
Запрос.УстановитьПараметр("ТЗ", ВнешняяТаблица);
Запрос.Текст = 
"ВЫБРАТЬ
|	ВЫБОР
|		КОГДА абс_ТипыСроковСтроительстваСоставКлсфИнтерфейс.Классификатор.ЭтоГруппа
|			ТОГДА абс_Классификатор.Ссылка
|		ИНАЧЕ абс_ТипыСроковСтроительстваСоставКлсфИнтерфейс.Классификатор
|	КОНЕЦ КАК Классификатор,
|	абс_ТипыСроковСтроительстваСоставКлсфИнтерфейс.Ссылка КАК РепернаяТочка
|ПОМЕСТИТЬ втКлассификаторТочка
|ИЗ
|	Справочник.абс_ТипыСроковСтроительства.СоставКлсфИнтерфейс КАК абс_ТипыСроковСтроительстваСоставКлсфИнтерфейс
|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.абс_Классификатор КАК абс_Классификатор
|		ПО абс_ТипыСроковСтроительстваСоставКлсфИнтерфейс.Классификатор = абс_Классификатор.Родитель
|ГДЕ
|	абс_ТипыСроковСтроительстваСоставКлсфИнтерфейс.Ссылка.ПометкаУдаления = ЛОЖЬ
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ РАЗЛИЧНЫЕ
|	абс_ВидыОбъектовИнвестицийПроектныеКлассификаторы.Ссылка КАК ВидОбъекта,
|	абс_ВидыОбъектовИнвестицийПроектныеКлассификаторы.Классификатор КАК Классификатор
|ПОМЕСТИТЬ втКлассификаторыПоВидуОбъекта
|ИЗ
|	Справочник.абс_ВидыОбъектовИнвестиций.ПроектныеКлассификаторы КАК абс_ВидыОбъектовИнвестицийПроектныеКлассификаторы
|ГДЕ
|	абс_ВидыОбъектовИнвестицийПроектныеКлассификаторы.Ссылка.ПометкаУдаления = ЛОЖЬ
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ
|	ТЗ.ТочкаКод КАК ТочкаКод,
|	ТЗ.КлассификаторКодификатор КАК КлассификаторКодификатор,
|	ТЗ.ОбъектGUID КАК ОбъектGUID,
|	ТЗ.ДатаНачала КАК ДатаНачала,
|	ТЗ.ДатаОкончания КАК ДатаОкончания,
|	ТЗ.ДатаНачалаБазовая КАК ДатаНачалаБазовая,
|	ТЗ.ДатаОкончанияБазовая КАК ДатаОкончанияБазовая
|ПОМЕСТИТЬ втТаблицаИмпорта
|ИЗ
|	&ТЗ КАК ТЗ
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ
|	ВЫБОР
|		КОГДА НЕ абс_ТипыСроковСтроительства.Ссылка ЕСТЬ NULL
|			ТОГДА ИСТИНА
|		ИНАЧЕ ЛОЖЬ
|	КОНЕЦ КАК УказанаРепернаяТочка,
|	абс_ТипыСроковСтроительства.Ссылка КАК ТипСрока,
|	абс_ОбъектыИнвестицийУУ.Ссылка КАК ОбъектИнвестиций,
|	абс_Классификатор.Ссылка КАК Классификатор,
|	втТаблицаИмпорта.ДатаНачала КАК ДатаНачала,
|	втТаблицаИмпорта.ДатаОкончания КАК ДатаОкончания,
|	втТаблицаИмпорта.ДатаНачалаБазовая КАК ДатаНачалаБазовая,
|	втТаблицаИмпорта.ДатаОкончанияБазовая КАК ДатаОкончанияБазовая
|ПОМЕСТИТЬ втИтог
|ИЗ
|	втТаблицаИмпорта КАК втТаблицаИмпорта
|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.абс_ТипыСроковСтроительства КАК абс_ТипыСроковСтроительства
|		ПО втТаблицаИмпорта.ТочкаКод = абс_ТипыСроковСтроительства.Код
|			И (НЕ абс_ТипыСроковСтроительства.ПометкаУдаления)
|			И (втТаблицаИмпорта.ТочкаКод <> """")
|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.абс_ОбъектыИнвестицийУУ КАК абс_ОбъектыИнвестицийУУ
|		ПО втТаблицаИмпорта.ОбъектGUID = абс_ОбъектыИнвестицийУУ.GUID
|			И (втТаблицаИмпорта.ОбъектGUID <> """")
|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.абс_Классификатор КАК абс_Классификатор
|		ПО втТаблицаИмпорта.КлассификаторКодификатор = абс_Классификатор.GUID
|			И (втТаблицаИмпорта.КлассификаторКодификатор <> """")
|ГДЕ
|	втТаблицаИмпорта.ОбъектGUID <> """"
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ РАЗЛИЧНЫЕ
|	втИтог.ТипСрока КАК ТипСрока,
|	втИтог.ОбъектИнвестиций КАК ОбъектИнвестиций,
|	абс_ОбъектыИнвестицийУУ.Ссылка КАК ОбъектИнвестицийОбщестроительныеРасходы,
|	втИтог.Классификатор КАК Классификатор,
|	втИтог.ДатаНачала КАК ДатаНачала,
|	втИтог.ДатаОкончания КАК ДатаОкончания,
|	втИтог.ДатаНачалаБазовая КАК ДатаНачалаБазовая,
|	втИтог.ДатаОкончанияБазовая КАК ДатаОкончанияБазовая,
|	втКлассификаторТочка.РепернаяТочка КАК РепернаяТочкаПоКлассификатору,
|	втИтог.УказанаРепернаяТочка КАК УказанаРепернаяТочка
|ПОМЕСТИТЬ втИсходныеДанные
|ИЗ
|	втИтог КАК втИтог
|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.абс_ОбъектыИнвестицийУУ КАК абс_ОбъектыИнвестицийУУ
|		ПО втИтог.ОбъектИнвестиций = абс_ОбъектыИнвестицийУУ.Родитель
|			И (абс_ОбъектыИнвестицийУУ.ВидОбъектаИнвестиции = ЗНАЧЕНИЕ(Справочник.абс_ВидыОбъектовИнвестиций.ОС_Корпус_общестроительные_расходы))
|		ЛЕВОЕ СОЕДИНЕНИЕ втКлассификаторТочка КАК втКлассификаторТочка
|		ПО втИтог.Классификатор = втКлассификаторТочка.Классификатор
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ
|	ВЫБОР
|		КОГДА втИсходныеДанные.РепернаяТочкаПоКлассификатору ЕСТЬ NULL
|			ТОГДА втИсходныеДанные.ТипСрока
|		ИНАЧЕ втИсходныеДанные.РепернаяТочкаПоКлассификатору
|	КОНЕЦ КАК ТипСрока,
|	ВЫБОР
|		КОГДА НЕ втИсходныеДанные.ОбъектИнвестицийОбщестроительныеРасходы ЕСТЬ NULL
|			ТОГДА втИсходныеДанные.ОбъектИнвестицийОбщестроительныеРасходы
|		ИНАЧЕ втИсходныеДанные.ОбъектИнвестиций
|	КОНЕЦ КАК ОбъектИнвестиций,
|	втИсходныеДанные.Классификатор КАК Классификатор,
|	втИсходныеДанные.ДатаНачала КАК ДатаНачала,
|	втИсходныеДанные.ДатаОкончания КАК ДатаОкончания,
|	втИсходныеДанные.ДатаНачалаБазовая КАК ДатаНачалаБазовая,
|	втИсходныеДанные.ДатаОкончанияБазовая КАК ДатаОкончанияБазовая,
|	втИсходныеДанные.РепернаяТочкаПоКлассификатору КАК РепернаяТочкаПоКлассификатору,
|	втИсходныеДанные.ОбъектИнвестиций КАК ОбъектИнвестицийИсходный,
|	втИсходныеДанные.ОбъектИнвестицийОбщестроительныеРасходы КАК ОбъектИнвестицийОбщестроительныеРасходы,
|	втИсходныеДанные.ТипСрока КАК ТипСрокаИсходный,
|	втИсходныеДанные.УказанаРепернаяТочка КАК УказанаРепернаяТочка
|ПОМЕСТИТЬ втДанныеЗаполненияДокумента
|ИЗ
|	втИсходныеДанные КАК втИсходныеДанные
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ РАЗЛИЧНЫЕ
|	втДанныеЗаполненияДокумента.ТипСрока КАК ТипСрока,
|	втДанныеЗаполненияДокумента.ОбъектИнвестиций КАК ОбъектИнвестиций,
|	втДанныеЗаполненияДокумента.Классификатор КАК Классификатор,
|	втДанныеЗаполненияДокумента.ДатаНачала КАК ДатаНачала,
|	втДанныеЗаполненияДокумента.ДатаОкончания КАК ДатаОкончания,
|	втДанныеЗаполненияДокумента.ДатаНачалаБазовая КАК ДатаНачалаБазовая,
|	втДанныеЗаполненияДокумента.ДатаОкончанияБазовая КАК ДатаОкончанияБазовая,
|	втДанныеЗаполненияДокумента.РепернаяТочкаПоКлассификатору КАК РепернаяТочкаПоКлассификатору,
|	втДанныеЗаполненияДокумента.ОбъектИнвестицийИсходный КАК ОбъектИнвестицийИсходный,
|	втДанныеЗаполненияДокумента.ОбъектИнвестицийОбщестроительныеРасходы КАК ОбъектИнвестицийОбщестроительныеРасходы,
|	втДанныеЗаполненияДокумента.ТипСрокаИсходный КАК ТипСрокаИсходный,
|	втДанныеЗаполненияДокумента.УказанаРепернаяТочка КАК УказанаРепернаяТочка
|ПОМЕСТИТЬ втДанныеЗаполненияДокументаИтог
|ИЗ
|	втДанныеЗаполненияДокумента КАК втДанныеЗаполненияДокумента
|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ втКлассификаторыПоВидуОбъекта КАК втКлассификаторыПоВидуОбъекта
|		ПО (втДанныеЗаполненияДокумента.Классификатор = втКлассификаторыПоВидуОбъекта.Классификатор
|				ИЛИ втДанныеЗаполненияДокумента.УказанаРепернаяТочка)
|			И втДанныеЗаполненияДокумента.ОбъектИнвестиций.ВидОбъектаИнвестиции = втКлассификаторыПоВидуОбъекта.ВидОбъекта
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ
|	втДанныеЗаполненияДокументаИтог.ТипСрока КАК ТипСрока,
|	втДанныеЗаполненияДокументаИтог.ОбъектИнвестиций КАК ОбъектИнвестиций,
|	втДанныеЗаполненияДокументаИтог.Классификатор КАК Классификатор,
|	МИНИМУМ(втДанныеЗаполненияДокументаИтог.ДатаНачала) КАК ДатаНачала,
|	МАКСИМУМ(втДанныеЗаполненияДокументаИтог.ДатаОкончания) КАК ДатаОкончания,
|	МИНИМУМ(втДанныеЗаполненияДокументаИтог.ДатаНачалаБазовая) КАК ДатаНачалаБазовая,
|	МАКСИМУМ(втДанныеЗаполненияДокументаИтог.ДатаОкончанияБазовая) КАК ДатаОкончанияБазовая,
|	втДанныеЗаполненияДокументаИтог.УказанаРепернаяТочка КАК УказанаРепернаяТочка
|ИЗ
|	втДанныеЗаполненияДокументаИтог КАК втДанныеЗаполненияДокументаИтог
|
|СГРУППИРОВАТЬ ПО
|	втДанныеЗаполненияДокументаИтог.ТипСрока,
|	втДанныеЗаполненияДокументаИтог.ОбъектИнвестиций,
|	втДанныеЗаполненияДокументаИтог.Классификатор,
|	втДанныеЗаполненияДокументаИтог.УказанаРепернаяТочка
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ
|	втДанныеЗаполненияДокументаИтог.ОбъектИнвестиций КАК ОбъектИнвестиций,
|	втКлассификаторТочка.РепернаяТочка КАК ТипСрока,
|	МИНИМУМ(втДанныеЗаполненияДокументаИтог.ДатаНачала) КАК ДатаНачала,
|	МАКСИМУМ(втДанныеЗаполненияДокументаИтог.ДатаОкончания) КАК ДатаОкончания,
|	МИНИМУМ(втДанныеЗаполненияДокументаИтог.ДатаНачалаБазовая) КАК ДатаНачалаБазовая,
|	МАКСИМУМ(втДанныеЗаполненияДокументаИтог.ДатаОкончанияБазовая) КАК ДатаОкончанияБазовая
|ИЗ
|	втДанныеЗаполненияДокументаИтог КАК втДанныеЗаполненияДокументаИтог
|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ втКлассификаторТочка КАК втКлассификаторТочка
|		ПО втДанныеЗаполненияДокументаИтог.Классификатор = втКлассификаторТочка.Классификатор
|ГДЕ
|	втДанныеЗаполненияДокументаИтог.УказанаРепернаяТочка = ЛОЖЬ
|
|СГРУППИРОВАТЬ ПО
|	втДанныеЗаполненияДокументаИтог.ОбъектИнвестиций,
|	втКлассификаторТочка.РепернаяТочка
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ РАЗЛИЧНЫЕ 
|	ВЫБОР
|		КОГДА втИсходныеДанные.ОбъектИнвестиций.Родитель.Родитель.Родитель <> ЗНАЧЕНИЕ(Справочник.абс_ОбъектыИнвестицийУУ.ПустаяСсылка)
|			ТОГДА втИсходныеДанные.ОбъектИнвестиций.Родитель.Родитель.Родитель
|		КОГДА втИсходныеДанные.ОбъектИнвестиций.Родитель.Родитель <> ЗНАЧЕНИЕ(Справочник.абс_ОбъектыИнвестицийУУ.ПустаяСсылка)
|			ТОГДА втИсходныеДанные.ОбъектИнвестиций.Родитель.Родитель
|		ИНАЧЕ втИсходныеДанные.ОбъектИнвестиций.Родитель
|	КОНЕЦ КАК ОбъектИнвестицийРодитель
|ИЗ
|	втИсходныеДанные КАК втИсходныеДанные
|ГДЕ
|	ВЫБОР
|			КОГДА втИсходныеДанные.ОбъектИнвестиций.Родитель.Родитель.Родитель <> ЗНАЧЕНИЕ(Справочник.абс_ОбъектыИнвестицийУУ.ПустаяСсылка)
|				ТОГДА втИсходныеДанные.ОбъектИнвестиций.Родитель.Родитель.Родитель
|			КОГДА втИсходныеДанные.ОбъектИнвестиций.Родитель.Родитель <> ЗНАЧЕНИЕ(Справочник.абс_ОбъектыИнвестицийУУ.ПустаяСсылка)
|				ТОГДА втИсходныеДанные.ОбъектИнвестиций.Родитель.Родитель
|			ИНАЧЕ втИсходныеДанные.ОбъектИнвестиций.Родитель
|		КОНЕЦ <> ЗНАЧЕНИЕ(Справочник.абс_ОбъектыИнвестицийУУ.ПустаяСсылка)";

УстановитьПривилегированныйРежим(Истина);
РезультатЗапроса = Запрос.ВыполнитьПакет();
УстановитьПривилегированныйРежим(Ложь);

ИндексРезультатаТаблицаЗаполнения = РезультатЗапроса.Количество() - 3;
ИндексРезультатаТаблицаЗаполненияТочекБезКлассификаторов = РезультатЗапроса.Количество() - 2;
ИндексРезультатаОС = РезультатЗапроса.Количество() - 1;

Если Не РезультатЗапроса[ИндексРезультатаТаблицаЗаполнения].Пустой() Тогда

	ВыборкаДетальныеЗаписи = РезультатЗапроса[ИндексРезультатаТаблицаЗаполнения].Выбрать();
	
	Если Не СоздатьДокумент Тогда
		ДокументГрафикОС = СсылкаДокумента.ПолучитьОбъект();
		ДокументГрафикОС.График.Очистить();
		ДокументГрафикОС.ОС.Очистить();
		ДокументГрафикОС.ОСИскл.Очистить();
	Иначе
		ДокументГрафикОС = Документы.абс_ГрафикСтроительстваОС.СоздатьДокумент();
		ДокументГрафикОС.УстановитьСсылкуНового(СсылкаДокумента);
	КонецЕсли;
	ДокументГрафикОС.Дата = ТекущаяДата();
	ДокументГрафикОС.УстановитьНовыйНомер();
	ДокументГрафикОС.Подпроект = ПроектСсылка;
	ДокументГрафикОС.Автор = ПользовательСсылка;
	
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		
		сшпОбщегоНазначения.ВыполнитьФункцию("ЕИС.Док.абс_ГрафикСтроительстваОС.ДобавитьСтрокуВГрафик", 
			ВыборкаДетальныеЗаписи, ДокументГрафикОС);
		
	КонецЦикла;
	
	ВыборкаДетальныеЗаписиТочекБезКлассификаторов = РезультатЗапроса[ИндексРезультатаТаблицаЗаполненияТочекБезКлассификаторов].Выбрать();
	Пока ВыборкаДетальныеЗаписиТочекБезКлассификаторов.Следующий() Цикл
		сшпОбщегоНазначения.ВыполнитьФункцию("ЕИС.Док.абс_ГрафикСтроительстваОС.ДобавитьСтрокуВГрафик", 
			ВыборкаДетальныеЗаписиТочекБезКлассификаторов, ДокументГрафикОС, Истина);
	КонецЦикла;

	ВыборкаОС = РезультатЗапроса[ИндексРезультатаОС].Выбрать();
	
	Пока ВыборкаОС.Следующий() Цикл
		НовСтр = ДокументГрафикОС.ОС.Добавить();
		НовСтр.Объект = ВыборкаОС.ОбъектИнвестицийРодитель;
	КонецЦикла;
	
	НачатьТранзакцию();
	
	Попытка
		
		ДокументГрафикОС.Записать();
		
		ЗаписьРегистраЗапросов = РегистрыСведений.сшпИнтеграцияЗапрошенныеИдентификаторы.СоздатьМенеджерЗаписи();
		ЗаписьРегистраЗапросов.ИдентификаторВнешний = СсылкаДокументаСтрокой;
		ЗаписьРегистраЗапросов.КлассСообщения = Число(ОбъектСообщение.ClassID);
		ЗаписьРегистраЗапросов.Узел = ОбъектСообщение.Source;
		ЗаписьРегистраЗапросов.Удалить();
		
		ЗафиксироватьТранзакцию();
		
	Исключение
		
		Если ТранзакцияАктивна() Тогда 
			ОтменитьТранзакцию();
		КонецЕсли;
		
		ВызватьИсключение;
		
	КонецПопытки;

Иначе

		ЗаписьРегистраЗапросов = РегистрыСведений.сшпИнтеграцияЗапрошенныеИдентификаторы.СоздатьМенеджерЗаписи();
		ЗаписьРегистраЗапросов.ИдентификаторВнешний = СсылкаДокументаСтрокой;
		ЗаписьРегистраЗапросов.КлассСообщения = Число(ОбъектСообщение.ClassID);
		ЗаписьРегистраЗапросов.Узел = ОбъектСообщение.Source;
		ЗаписьРегистраЗапросов.Удалить();
	
КонецЕсли;

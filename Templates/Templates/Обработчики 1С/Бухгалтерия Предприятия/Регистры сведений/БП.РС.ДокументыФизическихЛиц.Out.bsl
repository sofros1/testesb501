
СостояниеСообщения = ПредопределенноеЗначение("Перечисление.сшпСтатусыСообщений.ОтправкаПодтверждена");

Для Каждого ЗаписьРегистра Из ОбъектОбработки Цикл
	
	СтруктураЗапись = Новый Структура("Период, Физлицо, ВидДокумента, Серия, Номер, ДатаВыдачи, СрокДействия, КемВыдан, КодПодразделения, 
																	| ЯвляетсяДокументомУдостоверяющимЛичность, Представление, УдалитьВидДокумента, ФамилияЛатиницей, ИмяЛатиницей, СтранаВыдачи");
	ЗаполнитьЗначенияСвойств(СтруктураЗапись, ЗаписьРегистра);
	сшпРаботаСДанными.ПоместитьВОчередьИсходящих("РС.ДокументыФизическихЛиц", СтруктураЗапись);
	
КонецЦикла;

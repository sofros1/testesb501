
КлассыОбъектов = сшпОбщегоНазначения.ВыполнитьФункцию("БП.КлассыОбъектов");

xdtoОбъект = сшпОбщегоНазначения.ПолучитьОбъектXDTO(ФорматСообщения, ОбъектСообщение.Body);
xdtoСвойстваОбъекта = xdtoОбъект.Свойства();

сткСообщение = Новый Структура;
сткСообщение.Вставить("Узел", ОбъектСообщение.Source);
сткСообщение.Вставить("СостояниеСообщения", СостояниеСообщения);
сткСообщение.Вставить("ТекстОшибки", ТекстОшибки);
сткСообщение.Вставить("КоличествоПопытокОжидания", КоличествоПопытокОжидания);
сткСообщение.Вставить("Задержка", Задержка);
сткСообщение.Вставить("Отказ", Ложь);
сткСообщение.Вставить("СоответствиеНеНайденныхИдентификаторов", Новый Соответствие);
сткСообщение.Вставить("КлассыОбъектов", КлассыОбъектов);
сткСообщение.Вставить("xdtoОбъект", xdtoОбъект);
сткСообщение.Вставить("xdtoСвойстваОбъекта", xdtoСвойстваОбъекта);

НаборРегистра = РегистрыСведений.ДокументыФизическихЛиц.СоздатьНаборЗаписей();
новыйОбъект = НаборРегистра.Добавить();

СсылочныеРеквизиты = Новый Массив;
СсылочныеРеквизиты.Добавить("Физлицо");
СсылочныеРеквизиты.Добавить("ВидДокумента");
СсылочныеРеквизиты.Добавить("УдалитьВидДокумента");
СсылочныеРеквизиты.Добавить("СтранаВыдачи"); 

ЗначенияДоступныхРеквизитовОбъекта = сшпОбщегоНазначения.ВыполнитьФункцию("ЗначенияДоступныхРеквизитовОбъекта", xdtoОбъект, новыйОбъект, сткСообщение, СсылочныеРеквизиты);

ЗаполнитьЗначенияСвойств(новыйОбъект, ЗначенияДоступныхРеквизитовОбъекта);

НаборРегистра.Отбор.Физлицо.Установить(новыйОбъект.Физлицо);
НаборРегистра.Отбор.ВидДокумента.Установить(новыйОбъект.ВидДокумента);

////////////////////////////////////////////////////////////////////////////////
// Дозапрос недостающих данных

Если сткСообщение.СоответствиеНеНайденныхИдентификаторов.Количество() Тогда
	
	сшпИнтеграцияСервер.СформироватьЗапросНаПолучениеДанных(сткСообщение.СоответствиеНеНайденныхИдентификаторов, сткСообщение.Узел);
	
	Если сткСообщение.Отказ = Истина Тогда
		
		СостояниеСообщения = сткСообщение.СостояниеСообщения;
		Задержка = сткСообщение.Задержка;
		
		Перейти ~Выход;
		
	КонецЕсли;
	
КонецЕсли;

////////////////////////////////////////////////////////////////////////////////
//Запись объекта

НаборРегистра.УстановитьАктивность(Истина);
НаборРегистра.ОбменДанными.Загрузка = Истина;
НаборРегистра.Записать(Истина);
	
СостояниеСообщения = сткСообщение.СостояниеСообщения;
ТекстОшибки = сткСообщение.ТекстОшибки;
Задержка = сткСообщение.Задержка;

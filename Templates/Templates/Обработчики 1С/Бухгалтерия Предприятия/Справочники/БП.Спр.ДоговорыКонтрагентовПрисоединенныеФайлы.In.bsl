ТипОбъекта = "Справочник.ДоговорыКонтрагентовПрисоединенныеФайлы";

МенеджерОбъекта = ОбщегоНазначения.МенеджерОбъектаПоПолномуИмени(ТипОбъекта);

КлассыОбъектов = сшпОбщегоНазначения.ВыполнитьФункцию("БП.КлассыОбъектов");
КлассПакета = КлассыОбъектов.Получить(ТипОбъекта);

Если КлассПакета = Неопределено Тогда
	ВызватьИсключение СтрШаблон("Отсутствует описание соответствия класса типу для: %1", ТипОбъекта);
КонецЕсли;

xdtoОбъект = сшпОбщегоНазначения.ПолучитьОбъектXDTO(ФорматСообщения, ОбъектСообщение.Body);
xdtoСвойстваОбъекта = xdtoОбъект.Свойства();

Если сшпОбщегоНазначения.ВыполнитьФункцию("ОбъектРазрешенКЗагрузке", xdtoОбъект, ТипОбъекта) <> Истина Тогда
	Перейти ~Выход;
КонецЕсли;

////////////////////////////////////////////////////////////////////////////////
// Определение переменных

сткСообщение = Новый Структура;
сткСообщение.Вставить("Узел", ОбъектСообщение.Source);
сткСообщение.Вставить("СостояниеСообщения", СостояниеСообщения);
сткСообщение.Вставить("ТекстОшибки", ТекстОшибки);
сткСообщение.Вставить("КоличествоПопытокОжидания", КоличествоПопытокОжидания);
сткСообщение.Вставить("Задержка", Задержка);
сткСообщение.Вставить("Отказ", Ложь);
сткСообщение.Вставить("СоответствиеНеНайденныхИдентификаторов", Новый Соответствие);
сткСообщение.Вставить("ТипОбъекта", ТипОбъекта);
сткСообщение.Вставить("МенеджерОбъекта", МенеджерОбъекта);
сткСообщение.Вставить("КлассыОбъектов", КлассыОбъектов);
сткСообщение.Вставить("КлассПакета", КлассПакета);
сткСообщение.Вставить("xdtoОбъект", xdtoОбъект);
сткСообщение.Вставить("xdtoСвойстваОбъекта", xdtoСвойстваОбъекта);
сткСообщение.Вставить("ВнешняяСсылка", xdtoОбъект.Получить("Ссылка"));

СсылочныеРеквизиты = Новый Массив;
СсылочныеРеквизиты.Добавить("Автор");
СсылочныеРеквизиты.Добавить("ВладелецФайла");
СсылочныеРеквизиты.Добавить("Изменил");
СсылочныеРеквизиты.Добавить("Редактирует");
СсылочныеРеквизиты.Добавить("СтатусИзвлеченияТекста");
СсылочныеРеквизиты.Добавить("ТекстХранилище");
СсылочныеРеквизиты.Добавить("ТипХраненияФайла");
СсылочныеРеквизиты.Добавить("Том");
СсылочныеРеквизиты.Добавить("ФайлХранилище");
сткСообщение.Вставить("СсылочныеРеквизиты", СсылочныеРеквизиты);

РеквизитыДляПроверки = Новый Структура;
РеквизитыДляПроверки.Вставить("ИмяПредопределенныхДанных");
РеквизитыДляПроверки.Вставить("ЭтоГруппа");
ЗаполнитьЗначенияСвойств(РеквизитыДляПроверки, сткСообщение.xdtoОбъект);

// Обработка реквизитов для проверки

////////////////////////////////////////////////////////////////////////////////
// Поиск объекта

ЛокальныйИдентификатор = сшпИнтеграцияПовтИсп.ПолучитьЛокальныйИдентификатор(сткСообщение.ВнешняяСсылка, сткСообщение.КлассПакета, сткСообщение.Узел);
СсылкаНаЭлемент = МенеджерОбъекта.ПолучитьСсылку(Новый УникальныйИдентификатор(ЛокальныйИдентификатор));

// Дополнительные этапы поиска Объета

Если ОбщегоНазначения.СсылкаСуществует(СсылкаНаЭлемент) Тогда
	
	// Не замещать объект в приемнике 
	// Перейти ~Выход;

Иначе // Поиск по полям
	
	ПоляПоиска = Новый Массив;
	//Если ЗначениеЗаполнено(РеквизитыДляПроверки.ИмяПредопределенныхДанных) Тогда
	//	ПоляПоиска.Добавить("ИмяПредопределенныхДанных"); // 1
	//КонецЕсли;
	//ПоляПоиска.Добавить("GUID"); // 2
	//ПоляПоиска.Добавить("Наименование, Родитель, ЭтоГруппа"); // 3

	СсылкаПоПолям = сшпОбщегоНазначения.ВыполнитьФункцию("РезультатПоискаПоПолям", сткСообщение, ПоляПоиска);
	Если Не СсылкаПоПолям.Пустая() Тогда
		СсылкаНаЭлемент = СсылкаПоПолям;
	КонецЕсли;
	
	Если сткСообщение.Отказ = Истина Тогда
		
		СостояниеСообщения = сткСообщение.СостояниеСообщения;
		Задержка = сткСообщение.Задержка;
		
		Перейти ~Выход;
		
	КонецЕсли;
	
КонецЕсли;

Если ОбщегоНазначения.СсылкаСуществует(СсылкаНаЭлемент) тогда
	
	новыйОбъект = СсылкаНаЭлемент.ПолучитьОбъект();
	
Иначе

	Если РеквизитыДляПроверки.ЭтоГруппа = "true" Тогда
		новыйОбъект = МенеджерОбъекта.СоздатьГруппу();
	Иначе
		новыйОбъект = МенеджерОбъекта.СоздатьЭлемент();
	КонецЕсли;

	новыйОбъект.УстановитьСсылкуНового(СсылкаНаЭлемент);
	
КонецЕсли;

////////////////////////////////////////////////////////////////////////////////
//Реквизиты шапки

ЗначенияДоступныхРеквизитовОбъекта = сшпОбщегоНазначения.ВыполнитьФункцию("ЗначенияДоступныхРеквизитовОбъекта", xdtoОбъект, новыйОбъект, сткСообщение, СсылочныеРеквизиты);

ЗаполнитьЗначенияСвойств(новыйОбъект, ЗначенияДоступныхРеквизитовОбъекта);

// Дополнительные этапы заполнения реквизитов шапки

////////////////////////////////////////////////////////////////////////////////
// Табличные части

ИмяТЧ = "УдалитьЭлектронныеПодписи";
новыйОбъект[ИмяТЧ].Очистить();
текТаблица = xdtoОбъект[ИмяТЧ].Последовательность();
Для Инд = 0 По текТаблица.Количество()-1 Цикл

	xdtoСтрока = текТаблица.ПолучитьЗначение(Инд);
	текСтрока = новыйОбъект[ИмяТЧ].Добавить();

	ЗаполнитьЗначенияСвойств(текСтрока, xdtoСтрока);

	текСтрока.ДатаПодписи = сшпИнтеграцияПовтИсп.ПолучитьДату(xdtoСтрока.Получить("ДатаПодписи"));
	текСтрока.ДатаПроверкиПодписи = сшпИнтеграцияПовтИсп.ПолучитьДату(xdtoСтрока.Получить("ДатаПроверкиПодписи"));

	текСтрока.Подпись = сшпОбщегоНазначения.ВыполнитьФункцию("ЗначениеСсылкиИзСтруктуры", xdtoСтрока.Получить("Подпись"), сткСообщение);
	текСтрока.УстановившийПодпись = сшпОбщегоНазначения.ВыполнитьФункцию("ЗначениеСсылкиИзСтруктуры", xdtoСтрока.Получить("УстановившийПодпись"), сткСообщение);
	текСтрока.Сертификат = сшпОбщегоНазначения.ВыполнитьФункцию("ЗначениеСсылкиИзСтруктуры", xdtoСтрока.Получить("Сертификат"), сткСообщение);

КонецЦикла;

ИмяТЧ = "УдалитьСертификатыШифрования";
новыйОбъект[ИмяТЧ].Очистить();
текТаблица = xdtoОбъект[ИмяТЧ].Последовательность();
Для Инд = 0 По текТаблица.Количество()-1 Цикл

	xdtoСтрока = текТаблица.ПолучитьЗначение(Инд);
	текСтрока = новыйОбъект[ИмяТЧ].Добавить();

	ЗаполнитьЗначенияСвойств(текСтрока, xdtoСтрока);

	текСтрока.Сертификат = сшпОбщегоНазначения.ВыполнитьФункцию("ЗначениеСсылкиИзСтруктуры", xdtoСтрока.Получить("Сертификат"), сткСообщение);

КонецЦикла;


////////////////////////////////////////////////////////////////////////////////
// Дозапрос недостающих данных

Если сткСообщение.СоответствиеНеНайденныхИдентификаторов.Количество() Тогда
	
	сшпИнтеграцияСервер.СформироватьЗапросНаПолучениеДанных(сткСообщение.СоответствиеНеНайденныхИдентификаторов, сткСообщение.Узел);
	
	Если сткСообщение.Отказ = Истина Тогда
		
		СостояниеСообщения = сткСообщение.СостояниеСообщения;
		Задержка = сткСообщение.Задержка;
		
		Перейти ~Выход;
		
	КонецЕсли;
	
КонецЕсли;

////////////////////////////////////////////////////////////////////////////////
//Запись объекта

// Дополнительные этапы перед записью объекта

новыйОбъект.ОбменДанными.Загрузка = Истина;

сшпИнтеграцияСервер.ЗаписатьОбъектВБазуДанных(новыйОбъект, сткСообщение);

////////////////////////////////////////////////////////////////////////////////
// Переопределение контекстных переменных

СостояниеСообщения = сткСообщение.СостояниеСообщения;
ТекстОшибки = сткСообщение.ТекстОшибки;
Задержка = сткСообщение.Задержка;

РезультатОбработки.ClassId = 81;

Реквизиты = Новый Массив;
Реквизиты.Добавить("ИмяПредопределенныхДанных");
Реквизиты.Добавить("Предопределенный");
Реквизиты.Добавить("Ссылка");
Реквизиты.Добавить("ПометкаУдаления");
Реквизиты.Добавить("Владелец");
Реквизиты.Добавить("Наименование");
Реквизиты.Добавить("Код");
Реквизиты.Добавить("НомерДовер");
Реквизиты.Добавить("ДатаВыдачи");
Реквизиты.Добавить("ДатаОкончания");
Реквизиты.Добавить("ПризнакДоверителя");
Реквизиты.Добавить("ДоверительЮЛ_НаимОрг");
Реквизиты.Добавить("ДоверительЮЛ_ИНН");
Реквизиты.Добавить("ДоверительЮЛ_КПП");
Реквизиты.Добавить("ДоверительЮЛ_ОГРН");
Реквизиты.Добавить("ДоверительРук_ИНН");
Реквизиты.Добавить("ДоверительФЛ_ИНН");
Реквизиты.Добавить("ДоверительФЛ_ОГРН");
Реквизиты.Добавить("ДоверительФЛ_Гражданство");
Реквизиты.Добавить("ДоверительФЛ_ДатаРождения");
Реквизиты.Добавить("ПредставительЮЛ_НаимОрг");
Реквизиты.Добавить("ПредставительЮЛ_ИНН");
Реквизиты.Добавить("ПредставительЮЛ_КПП");
Реквизиты.Добавить("ПредставительЮЛ_ОГРН");
Реквизиты.Добавить("ПредставительФЛ_ИНН");
Реквизиты.Добавить("ПредставительФЛ_ОГРН");
Реквизиты.Добавить("ПредставительФЛ_Гражданство");
Реквизиты.Добавить("ПредставительФЛ_ДатаРождения");
Реквизиты.Добавить("НотариусЮЛ_НаимОрг");
Реквизиты.Добавить("НотариусЮЛ_ИНН");
Реквизиты.Добавить("НотариусЮЛ_КПП");
Реквизиты.Добавить("НотариусЮЛ_ОГРН");
Реквизиты.Добавить("НотариусФЛ_ИНН");
Реквизиты.Добавить("ДоверительЯвляетсяЮЛ");
Реквизиты.Добавить("ПредставительЯвляетсяЮЛ");
Реквизиты.Добавить("НотариусЯвляетсяЮЛ");
Реквизиты.Добавить("ЗаверенаНотариально");
Реквизиты.Добавить("ДоверительИмеетУЛ");
Реквизиты.Добавить("ПредставительЯвляетсяСотрудником");
// Табличные части
Реквизиты.Добавить("ПолномочияПредставителя");
Реквизиты.Добавить("ФИО");
Реквизиты.Добавить("Адреса");
Реквизиты.Добавить("УдЛичности");

Данные = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ОбъектОбработки.Ссылка, Реквизиты);

////////////////////////////////////////////////////////////////////////////////
// Подготовка данных

// Обработка ссылочных реквизитов
СсылочныеРеквизиты = Новый Массив;
СсылочныеРеквизиты.Добавить("Владелец"); // Справочник.Организации
СсылочныеРеквизиты.Добавить("ДоверительФЛ_Гражданство"); // Справочник.СтраныМира
СсылочныеРеквизиты.Добавить("ПредставительФЛ_Гражданство"); // Справочник.СтраныМира

Для каждого Реквизит Из СсылочныеРеквизиты Цикл
	Данные.Вставить(Реквизит, ВыполнитьФункцию("СтруктураЗначенияСсылки", Данные[Реквизит]));
КонецЦикла;

// Обработка табличных частей
ИмяТЧ = "ПолномочияПредставителя";
Данные.Вставить(ИмяТЧ, ОбщегоНазначения.ТаблицаЗначенийВМассив(Данные[ИмяТЧ].Выгрузить()));

ИмяТЧ = "ФИО";
Данные.Вставить(ИмяТЧ, ОбщегоНазначения.ТаблицаЗначенийВМассив(Данные[ИмяТЧ].Выгрузить()));
Для Каждого текСтрока Из Данные[ИмяТЧ] Цикл
	текСтрока.Вставить("Владелец", ВыполнитьФункцию("СтруктураЗначенияСсылки", текСтрока.Владелец));  
КонецЦикла;

ИмяТЧ = "Адреса";
Данные.Вставить(ИмяТЧ, ОбщегоНазначения.ТаблицаЗначенийВМассив(Данные[ИмяТЧ].Выгрузить()));
Для Каждого текСтрока Из Данные[ИмяТЧ] Цикл
	текСтрока.Вставить("Владелец", ВыполнитьФункцию("СтруктураЗначенияСсылки", текСтрока.Владелец));  
КонецЦикла;

ИмяТЧ = "УдЛичности";
Данные.Вставить(ИмяТЧ, ОбщегоНазначения.ТаблицаЗначенийВМассив(Данные[ИмяТЧ].Выгрузить()));
Для Каждого текСтрока Из Данные[ИмяТЧ] Цикл
	текСтрока.Вставить("Владелец", ВыполнитьФункцию("СтруктураЗначенияСсылки", текСтрока.Владелец));  
	текСтрока.Вставить("ВидДок", ВыполнитьФункцию("СтруктураЗначенияСсылки", текСтрока.ВидДок)); // Справочник.ВидыДокументовФизическихЛиц
КонецЦикла;

////////////////////////////////////////////////////////////////////////////////
// Отправка данных
РезультатОбработки.Body = сшпОбщегоНазначения.ПреобразоватьСтруктуруПоФормату(ФорматСообщения, Данные);

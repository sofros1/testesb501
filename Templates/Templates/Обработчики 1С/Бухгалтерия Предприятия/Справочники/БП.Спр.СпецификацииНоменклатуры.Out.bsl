РезультатОбработки.ClassId = 36;

Реквизиты = Новый Массив;
Реквизиты.Добавить("ИмяПредопределенныхДанных");
Реквизиты.Добавить("Предопределенный");
Реквизиты.Добавить("Ссылка");
Реквизиты.Добавить("ПометкаУдаления");
Реквизиты.Добавить("ЭтоГруппа");
Реквизиты.Добавить("Владелец");
Реквизиты.Добавить("Родитель");
Реквизиты.Добавить("Наименование");
Реквизиты.Добавить("Код");
Реквизиты.Добавить("Количество");
// Табличные части
Реквизиты.Добавить("ИсходныеКомплектующие");

Данные = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ОбъектОбработки.Ссылка, Реквизиты);

////////////////////////////////////////////////////////////////////////////////
// Подготовка данных

// Обработка ссылочных реквизитов
СсылочныеРеквизиты = Новый Массив;
СсылочныеРеквизиты.Добавить("Владелец"); // Справочник.Номенклатура
СсылочныеРеквизиты.Добавить("Родитель"); // Справочник.СпецификацииНоменклатуры

Для каждого Реквизит Из СсылочныеРеквизиты Цикл
	Данные.Вставить(Реквизит, ВыполнитьФункцию("СтруктураЗначенияСсылки", Данные[Реквизит]));
КонецЦикла;

// Обработка табличных частей
ИмяТЧ = "ИсходныеКомплектующие";
Данные.Вставить(ИмяТЧ, ОбщегоНазначения.ТаблицаЗначенийВМассив(Данные[ИмяТЧ].Выгрузить()));
Для Каждого текСтрока Из Данные[ИмяТЧ] Цикл
	текСтрока.Вставить("Номенклатура", ВыполнитьФункцию("СтруктураЗначенияСсылки", текСтрока.Номенклатура)); // Справочник.Номенклатура
КонецЦикла;

////////////////////////////////////////////////////////////////////////////////
// Отправка данных
РезультатОбработки.Body = сшпОбщегоНазначения.ПреобразоватьСтруктуруПоФормату(ФорматСообщения, Данные);

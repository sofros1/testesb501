
СвойстваСообщения = ОбъектСообщение.Properties;	
	
Если ТипЗнч(СвойстваСообщения) = Тип("ОбъектXDTO") Тогда
	СвойстваСообщения = сшпОбщегоНазначения.ПолучитьПараметрыСообщенияСтруктурой(ОбъектСообщение);
КонецЕсли;

Если Не ТипЗнч(СвойстваСообщения) = Тип("Структура") Тогда
	ВызватьИсключение "Некорректный формат свойств сообщения";
КонецЕсли;

сткСообщение = Новый Структура;
сткСообщение.Вставить("СостояниеСообщения", СостояниеСообщения);
сткСообщение.Вставить("КоличествоПопытокОжидания", КоличествоПопытокОжидания);
сткСообщение.Вставить("Задержка", Задержка);
сткСообщение.Вставить("Отказ", Ложь);

ЧтениеJSON = Новый ЧтениеJSON();
ЧтениеJSON.УстановитьСтроку(ОбъектСообщение.Body);
ТелоСообщения = ПрочитатьJSON(ЧтениеJSON, Ложь);	

Для Каждого ЭлементДанных Из ТелоСообщения Цикл
	
	ЗаписьРегистраСоответствия = РегистрыСведений.сшпСоответствиеИдентификаторов.СоздатьМенеджерЗаписи();
	ЗаписьРегистраСоответствия.ИдентификаторВнешний = ЭлементДанных["ExtID"];
	ЗаписьРегистраСоответствия.ИдентификаторЛокальный = ЭлементДанных["ElmaID"];
	ЗаписьРегистраСоответствия.Узел = "ELMA";
	ЗаписьРегистраСоответствия.КлассСообщения = 274;
	ЗаписьРегистраСоответствия.Записать();
	
КонецЦикла;

ТипОбъекта = "Справочник.ФизическиеЛица";

МенеджерОбъекта = ОбщегоНазначения.МенеджерОбъектаПоПолномуИмени(ТипОбъекта);

ТаблицаСоответствийКлассовИОбъектов = сшпОбщегоНазначения.ВыполнитьФункцию("ProjectsNDK.ТаблицаСоответствийКлассовИОбъектов");
КлассыОбъектов = сшпОбщегоНазначения.ВыполнитьФункцию("КлассыОбъектов", ТаблицаСоответствийКлассовИОбъектов);
КлассПакета = 276;

xdtoОбъект = сшпОбщегоНазначения.ПолучитьОбъектXDTO(ФорматСообщения, ОбъектСообщение.Body);
xdtoСвойстваОбъекта = xdtoОбъект.Свойства();

Если сшпОбщегоНазначения.ВыполнитьФункцию("ОбъектРазрешенКЗагрузке", xdtoОбъект, ТипОбъекта) <> Истина Тогда
	Перейти ~Выход;
КонецЕсли;

СвойстваСообщения = сшпОбщегоНазначения.ПолучитьПараметрыСообщенияСтруктурой(ОбъектСообщение);

DoNotReplaceObject = Неопределено;
СвойстваСообщения.Свойство("DoNotReplaceObject", DoNotReplaceObject);

////////////////////////////////////////////////////////////////////////////////
// Определение переменных

сткСообщение = Новый Структура;
сткСообщение.Вставить("Узел", ОбъектСообщение.Source);
сткСообщение.Вставить("СостояниеСообщения", СостояниеСообщения);
сткСообщение.Вставить("ТекстОшибки", ТекстОшибки);
сткСообщение.Вставить("КоличествоПопытокОжидания", КоличествоПопытокОжидания);
сткСообщение.Вставить("Задержка", Задержка);
сткСообщение.Вставить("Отказ", Ложь);
сткСообщение.Вставить("СоответствиеНеНайденныхИдентификаторов", Новый Соответствие);
сткСообщение.Вставить("ТипОбъекта", ТипОбъекта);
сткСообщение.Вставить("МенеджерОбъекта", МенеджерОбъекта);
сткСообщение.Вставить("КлассыОбъектов", КлассыОбъектов);
сткСообщение.Вставить("КлассПакета", КлассПакета);
сткСообщение.Вставить("xdtoОбъект", xdtoОбъект);
сткСообщение.Вставить("xdtoСвойстваОбъекта", xdtoСвойстваОбъекта);
сткСообщение.Вставить("ВнешняяСсылка", xdtoОбъект.Получить("Ссылка"));

СсылочныеРеквизиты = Новый Массив;
//СсылочныеРеквизиты.Добавить("Родитель");
сткСообщение.Вставить("СсылочныеРеквизиты", СсылочныеРеквизиты);

РеквизитыДляПроверки = Новый Структура;
РеквизитыДляПроверки.Вставить("ИмяПредопределенныхДанных");
РеквизитыДляПроверки.Вставить("ЭтоГруппа");
ЗаполнитьЗначенияСвойств(РеквизитыДляПроверки, сткСообщение.xdtoОбъект);

// Обработка реквизитов для проверки

////////////////////////////////////////////////////////////////////////////////
// Поиск объекта

ЛокальныйИдентификатор = сшпИнтеграцияПовтИсп.ПолучитьЛокальныйИдентификатор(сткСообщение.ВнешняяСсылка, сткСообщение.КлассПакета, сткСообщение.Узел);
СсылкаНаЭлемент = МенеджерОбъекта.ПолучитьСсылку(Новый УникальныйИдентификатор(ЛокальныйИдентификатор));

// Дополнительные этапы поиска Объета

Если ОбщегоНазначения.СсылкаСуществует(СсылкаНаЭлемент) Тогда
	
	Если DoNotReplaceObject = Истина Тогда
	//	Не замещать объект в приемнике 
	//	Перейти ~Выход;
	КонецЕсли;

Иначе // Поиск по полям
	
	ПоляПоиска = Новый Массив;
	//Если ЗначениеЗаполнено(РеквизитыДляПроверки.ИмяПредопределенныхДанных) Тогда
	//	ПоляПоиска.Добавить("ИмяПредопределенныхДанных"); // 1
	//КонецЕсли;
	//ПоляПоиска.Добавить("GUID"); // 2
	//ПоляПоиска.Добавить("Наименование, Родитель, ЭтоГруппа"); // 3

	СсылкаПоПолям = сшпОбщегоНазначения.ВыполнитьФункцию("РезультатПоискаПоПолям", сткСообщение, ПоляПоиска);
	Если Не СсылкаПоПолям.Пустая() Тогда
		СсылкаНаЭлемент = СсылкаПоПолям;
	КонецЕсли;
	
	Если сткСообщение.Отказ = Истина Тогда
		
		СостояниеСообщения = сткСообщение.СостояниеСообщения;
		Задержка = сткСообщение.Задержка;
		
		Перейти ~Выход;
		
	КонецЕсли;
	
КонецЕсли;

Если ОбщегоНазначения.СсылкаСуществует(СсылкаНаЭлемент) тогда
	
	новыйОбъект = СсылкаНаЭлемент.ПолучитьОбъект();
	
Иначе

	Перейти ~Выход; //Пользователей получаем из ЗУПа
	
КонецЕсли;

////////////////////////////////////////////////////////////////////////////////
//Реквизиты шапки

// Дополнительные этапы заполнения реквизитов шапки

////////////////////////////////////////////////////////////////////////////////
// Табличные части

Если РеквизитыДляПроверки.ЭтоГруппа = "false" Тогда

	ИмяТЧ = "КонтактнаяИнформация";
	текТаблица = xdtoОбъект[ИмяТЧ].Последовательность();
	
	ТаблицаКИ = Новый ТаблицаЗначений;
	ТаблицаКИ.Колонки.Добавить("Вид", Новый ОписаниеТипов("СправочникСсылка.ВидыКонтактнойИнформации"));
	ТаблицаКИ.Колонки.Добавить("Значение", Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(1024)));
	
	СоответствиеКИ = Новый Соответствие;
	СоответствиеКИ.Вставить("mail", ПредопределенноеЗначение("Справочник.ВидыКонтактнойИнформации.EMailФизическиеЛица"));
	СоответствиеКИ.Вставить("telephoneNumber", ПредопределенноеЗначение("Справочник.ВидыКонтактнойИнформации.РабочийТелефонФизическиеЛица"));
	
	МассивВидовКИ = Новый Массив;
	Для Каждого КлючЗначениеКИ Из СоответствиеКИ Цикл 
		МассивВидовКИ.Добавить(КлючЗначениеКИ.Значение);
	КонецЦикла;
	
	Для Инд = 0 По текТаблица.Количество()-1 Цикл

		xdtoСтрока = текТаблица.ПолучитьЗначение(Инд);

		ВидКИ = xdtoСтрока.Получить("Вид");
		ЗначениеКИ = xdtoСтрока.Получить("Значение");
		ВидКИСсылка = СоответствиеКИ.Получить(ВидКИ);
		
		Если ВидКИСсылка <> Неопределено И Не ПустаяСтрока(ЗначениеКИ) Тогда 
			
			СтрокаТаблицыКИ = ТаблицаКИ.Добавить();
			СтрокаТаблицыКИ.Вид = ВидКИСсылка;
			СтрокаТаблицыКИ.Значение = ЗначениеКИ;
			
		КонецЕсли;

	КонецЦикла;
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ВидыКонтактнойИнформации.Ссылка КАК Ссылка
		|ПОМЕСТИТЬ ВТ_ЗагружаемыеВиды
		|ИЗ
		|	Справочник.ВидыКонтактнойИнформации КАК ВидыКонтактнойИнформации
		|ГДЕ
		|	ВидыКонтактнойИнформации.Ссылка В(&МассивВидовКИ)
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	*
		|ПОМЕСТИТЬ ВТ_ТЧ
		|ИЗ
		|	&ТЧ КАК ТЧ
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	*
		|ПОМЕСТИТЬ ВТ_ТЧ_КИ
		|ИЗ
		|	&ТЗ_КИ КАК ТЗ_КИ
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТ_ТЧ.*
		|ИЗ
		|	ВТ_ТЧ КАК ВТ_ТЧ
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВТ_ЗагружаемыеВиды КАК ВТ_ЗагружаемыеВиды
		|		ПО ВТ_ТЧ.Вид = ВТ_ЗагружаемыеВиды.Ссылка
		|ГДЕ
		|	ВТ_ЗагружаемыеВиды.Ссылка ЕСТЬ NULL
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТ_ТЧ_КИ.*
		|ИЗ
		|	ВТ_ТЧ_КИ КАК ВТ_ТЧ_КИ
		|";
	
	Запрос.УстановитьПараметр("МассивВидовКИ", МассивВидовКИ);
	Запрос.УстановитьПараметр("ТЧ", новыйОбъект.КонтактнаяИнформация.Выгрузить());
	Запрос.УстановитьПараметр("ТЗ_КИ", ТаблицаКИ);	
	
	ПакетЗапроса = Запрос.ВыполнитьПакет();
	
	новыйОбъект.КонтактнаяИнформация.Загрузить(ПакетЗапроса[3].Выгрузить());
	
	ВыборкаКИ = ПакетЗапроса[4].Выбрать();
	Пока ВыборкаКИ.Следующий() Цикл
		
		УправлениеКонтактнойИнформацией.ДобавитьКонтактнуюИнформацию(новыйОбъект, ВыборкаКИ.Значение, 
			ВыборкаКИ.Вид, , Ложь);
				
	КонецЦикла;
			
КонецЕсли;

////////////////////////////////////////////////////////////////////////////////
// Дозапрос недостающих данных

// Если сткСообщение.СоответствиеНеНайденныхИдентификаторов.Количество() Тогда
// 
// сшпИнтеграцияСервер.СформироватьЗапросНаПолучениеДанных(сткСообщение.СоответствиеНеНайденныхИдентификаторов, сткСообщение.Узел);
// 
// Если сткСообщение.Отказ = Истина Тогда
// 
// СостояниеСообщения = сткСообщение.СостояниеСообщения;
// Задержка = сткСообщение.Задержка;
// 
// Перейти ~Выход;
// 
// КонецЕсли;
// 
// КонецЕсли;

////////////////////////////////////////////////////////////////////////////////
//Запись объекта

// Дополнительные этапы перед записью объекта

сшпИнтеграцияСервер.ЗаписатьОбъектВБазуДанных(новыйОбъект, сткСообщение);

////////////////////////////////////////////////////////////////////////////////
// Переопределение контекстных переменных

СостояниеСообщения = сткСообщение.СостояниеСообщения;
ТекстОшибки = сткСообщение.ТекстОшибки;
Задержка = сткСообщение.Задержка;

	РезультатОбработки.ClassId = 303;

	Properties = Новый Структура("OwnerType, OwnerId, Name, Extension, URL");
	Properties.Вставить("to_ASOSZ", Истина);
    ЗаполнитьЗначенияСвойств(Properties, ОбъектОбработки, "OwnerType, OwnerId, Name, Extension, URL"); 
	
	РезультатОбработки.Properties = Properties;

	РезультатОбработки.Body = сшпОбщегоНазначения.ПреобразоватьСтруктуруПоФормату(ФорматСообщения, ОбъектОбработки);

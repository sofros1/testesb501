РезультатОбработки.ClassId = 305;

Реквизиты = Новый Структура;
Реквизиты.Вставить("Ссылка");
Реквизиты.Вставить("ПометкаУдаления");
Реквизиты.Вставить("Родитель");
Реквизиты.Вставить("Наименование");
Реквизиты.Вставить("Код");
// Табличные части


Данные = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ОбъектОбработки.Ссылка, Реквизиты);

////////////////////////////////////////////////////////////////////////////////
// Подготовка данных

// Обработка ссылочных реквизитов


// Обработка табличных частей

//Свойства
Properties = Новый Структура;
Properties.Вставить("toPlatform", Истина);

РезультатОбработки.Properties = Properties;

////////////////////////////////////////////////////////////////////////////////
// Отправка данных
РезультатОбработки.Body = сшпОбщегоНазначения.ПреобразоватьСтруктуруПоФормату(ФорматСообщения, Данные);

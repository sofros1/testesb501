	СвойстваСообщения = ОбъектСообщение.Properties;
	Если ТипЗнч(СвойстваСообщения) = Тип("ОбъектXDTO") Тогда
		СвойстваСообщения = сшпОбщегоНазначения.ПолучитьПараметрыСообщенияСтруктурой(ОбъектСообщение);
	КонецЕсли;

	Если Не ТипЗнч(СвойстваСообщения) = Тип("Структура") Тогда
		ВызватьИсключение "Некорректный формат свойств сообщения";
	КонецЕсли;
		
	ИменаСвойствСоЗначениемДата = Новый Массив;
	ИменаСвойствСоЗначениемДата.Добавить("created_at");
	ИменаСвойствСоЗначениемДата.Добавить("take_date");
	ИменаСвойствСоЗначениемДата.Добавить("updated_at");
	
	ЧтениеJSON = Новый ЧтениеJSON();
	ЧтениеJSON.УстановитьСтроку(ОбъектСообщение.Body); 
	ТелоСообщения = ПрочитатьJSON(ЧтениеJSON, Ложь, ИменаСвойствСоЗначениемДата);     
	
	ЭтоДозапросПоПомещению = СвойстваСообщения.Свойство("messageType") И СвойстваСообщения.messageType = "Room_embed"; 
		
	Если ЭтоДозапросПоПомещению Тогда  
		
		Если Не ТелоСообщения.Свойство("custom_fields") Тогда
			ВызватьИсключение "В дозапросе по помещению должно быть поле custom_fields";
		КонецЕсли;    
		
		ДополнительныеПоля = ТелоСообщения.custom_fields;
		
		Если Не ЗначениеЗаполнено(ДополнительныеПоля) Тогда
			ЕстьАктДопускаВПомещение = "0";
			ДатаАктаДопускаВПомещение = "";
		Иначе    			
		
			ЕстьАктДопускаВПомещение = Неопределено;
			ДатаАктаДопускаВПомещение = Неопределено;
			
			Для Каждого ДопПоле Из ДополнительныеПоля Цикл  
				Если ДопПоле.code = "certificate_admission" Тогда 
					ЕстьАктДопускаВПомещение = ДопПоле.value;
				ИначеЕсли ДопПоле.code = "data_advp" Тогда
					ДатаАктаДопускаВПомещение = ?(ДопПоле.value = Неопределено, "", ДопПоле.value);
				КонецЕсли;
			КонецЦикла;			
			
			Если ЕстьАктДопускаВПомещение = Неопределено И ДатаАктаДопускаВПомещение = Неопределено Тогда 
				ЕстьАктДопускаВПомещение = "0";
				ДатаАктаДопускаВПомещение = "";	
			Иначе
			
				Если ЕстьАктДопускаВПомещение = Неопределено Тогда 
					ВызватьИсключение "В доп. полях должно быть свойство 'certificate_admission' (Акт допуска в помещение)";
				ИначеЕсли ДатаАктаДопускаВПомещение = Неопределено Тогда 
					ВызватьИсключение "В доп. полях должно быть свойство 'data_advp' (Дата акта допуска в помещение)";
				КонецЕсли;
				
			КонецЕсли;	
			
		КонецЕсли;
		
		//Вместо повторного запроса можно было в параметре передавать json приемки. 
		Структура = Новый Структура();
		Структура.Вставить("replyMessageClass", 121); //Bazis.Inspections
		Структура.Вставить("messageType", "Inspection_embed"); 
		Структура.Вставить("url", СтрШаблон("/inspections/%1", Формат(СвойстваСообщения.inspectionId, "ЧГ=0")));
		Структура.Вставить("certificate_admission", ЕстьАктДопускаВПомещение);
		Структура.Вставить("data_advp", ДатаАктаДопускаВПомещение);
		сшпПользовательскиеМетоды.ПоместитьВОчередьИсходящих("Bazis.Embed.Request", Структура);

		Перейти ~Выход;
		
	КонецЕсли;
	
	Статусы = сшпОбщегоНазначения.ВыполнитьФункцию("СтатусыBazis", "Inspection");
		
	Если ТелоСообщения.status_id = Статусы.Новая Или ТелоСообщения.status_id = Статусы.ВзятаВРаботу Тогда 		
					
		Помещение = сшпОбщегоНазначения.ВыполнитьФункцию("ЕСЭД.Объект_ИД_Базис", ТелоСообщения.room_id, "Справочник.АБС_Помещения");
		
		Если Не ЗначениеЗаполнено(Помещение) Тогда 
			ВызватьИсключение СтрШаблон("ЕСЭД.Bazis.Inspections.In: Не найдено помещение по 'room_id'", ТелоСообщения.room_id);                                  
		КонецЕсли;  			
				
		Запрос = Новый Запрос;
		Запрос.Текст = 
			"ВЫБРАТЬ
			|	СтатусыПередачиОбъектовСрезПоследних.Период КАК ДатаПриемки,
			|	СтатусыПередачиОбъектовСрезПоследних.СтатусПередачи КАК СтатусПередачи
			|ИЗ
			|	РегистрСведений.АБС_СтатусыПередачиОбъектов.СрезПоследних(, Помещение = &Помещение) КАК СтатусыПередачиОбъектовСрезПоследних
			|ГДЕ
			|	СтатусыПередачиОбъектовСрезПоследних.СтатусПередачи = ЗНАЧЕНИЕ(Перечисление.АБС_СтатусыПередачиОбъектов.ВПроцессеПриемки)";
		
		Запрос.УстановитьПараметр("Помещение", Помещение);
		
		РезультатЗапроса = Запрос.Выполнить();	
		
		Выборка = РезультатЗапроса.Выбрать();
		
		Если Выборка.Следующий() Тогда   
			
			ДатаПриемкиЖКХ = Выборка.ДатаПриемки;
			
			ДатаПриемкиБазис = ?(ТелоСообщения.status_id = Статусы.Новая, ТелоСообщения.take_date, ТекущаяДатаСеанса()); 
						
			Если ДатаПриемкиБазис <> ДатаПриемкиЖКХ Тогда
				
				//Удаляем старую запись
				НаборЗаписей = РегистрыСведений.АБС_СтатусыПередачиОбъектов.СоздатьНаборЗаписей();
				НаборЗаписей.Отбор.Помещение.Установить(Помещение);
				НаборЗаписей.Отбор.Период.Установить(ДатаПриемкиЖКХ); 
				НаборЗаписей.Записать();
				
				//добавляем новую запись
				НовЗапись = РегистрыСведений.АБС_СтатусыПередачиОбъектов.СоздатьМенеджерЗаписи();
				НовЗапись.Период = ДатаПриемкиБазис;
				НовЗапись.Помещение = Помещение;
				НовЗапись.СтатусПередачи = Перечисления.АБС_СтатусыПередачиОбъектов.ВПроцессеПриемки;
				НовЗапись.Записать();
				
			КонецЕсли;
			
		КонецЕсли;			
						
	ИначеЕсли ТелоСообщения.status_id = Статусы.НаРассметрении Тогда  
		
		ВхОбращение = сшпОбщегоНазначения.ВыполнитьФункцию("ЕСЭД.Объект_ИД_Базис", ТелоСообщения.id, "Справочник.ВходящиеДокументы");
		
		Если ЗначениеЗаполнено(ВхОбращение) Тогда 
			Перейти ~Выход;
		КонецЕсли;
		
		Помещение = сшпОбщегоНазначения.ВыполнитьФункцию("ЕСЭД.Объект_ИД_Базис", ТелоСообщения.room_id, "Справочник.АБС_Помещения");
		
		Если Не ЗначениеЗаполнено(Помещение) Тогда 
			ВызватьИсключение СтрШаблон("ЕСЭД.Bazis.Inspections.In: Не найдено помещение по 'room_id'", ТелоСообщения.room_id);
		КонецЕсли; 	
		
		ПараметрыФункции = Новый Структура();
		ПараметрыФункции.Вставить("ИдДокумента", ТелоСообщения.id);
		ПараметрыФункции.Вставить("Дата", ТелоСообщения.created_at);
		ПараметрыФункции.Вставить("Помещение", Помещение);
		ПараметрыФункции.Вставить("Статус", ПредопределенноеЗначение("Справочник.АБС_СтатусыОбращений.НаРассмотрении"));
		ПараметрыФункции.Вставить("ПредметОбращения", АБС_СвойстваПовтИсп.ПолучитьСсылкуПоНаименованию("АБС_ПредметыОбращений.ОтказОтПриемки(НесогласиеСЗамечаниямиКлиента)")); 
		ВхОбращение = сшпОбщегоНазначения.ВыполнитьФункцию("ЕСЭД.СоздатьВходящееОбращение", ПараметрыФункции); 
				
		РегистрыСведений.АБС_СтатусыПередачиОбъектов.ДобавитьЗапись(ТекущаяДатаСеанса(), Помещение, ПредопределенноеЗначение("Перечисление.АБС_СтатусыПередачиОбъектов.ОтказОтПриемки"));		
				
	ИначеЕсли ТелоСообщения.status_id = Статусы.ПринятаБезЗамечаний Или ТелоСообщения.status_id = Статусы.ПринятаСЗамечаниями Тогда
						
		ЕстьДанныеПоАктуДокускаВПомещение = СвойстваСообщения.Свойство("certificate_admission") И СвойстваСообщения.Свойство("data_advp");
			
		Если Не ЕстьДанныеПоАктуДокускаВПомещение Тогда 
			
			Структура = Новый Структура();
			Структура.Вставить("replyMessageClass", 121); //Bazis.Inspections
			Структура.Вставить("messageType", "Room_embed");
			Структура.Вставить("url", СтрШаблон("/rooms/%1?embed=custom_fields", Формат(ТелоСообщения.room_id, "ЧГ=0"))); //БЯ 29.08.23
			Структура.Вставить("inspectionId", ТелоСообщения.id); 
			сшпПользовательскиеМетоды.ПоместитьВОчередьИсходящих("Bazis.Embed.Request", Структура);	
			
			Перейти ~Выход;
			
		КонецЕсли;
		
		ВхОбращение = сшпОбщегоНазначения.ВыполнитьФункцию("ЕСЭД.Объект_ИД_Базис", ТелоСообщения.id, "Справочник.ВходящиеДокументы");
		
		Если ЗначениеЗаполнено(ВхОбращение) Тогда 
		
			ВхОбращениеОб = ВхОбращение.ПолучитьОбъект();
			ВхОбращениеОб.АБС_Статус = ПредопределенноеЗначение("Справочник.АБС_СтатусыОбращений.Отклонен");
			ВхОбращениеОб.АБС_СпособОтвета = ПредопределенноеЗначение("Перечисление.АБС_СпособПредоставленияОтвета.ОтветНеТребуется");
			ВхОбращениеОб.Записать();
			
			сшпОбщегоНазначения.ВыполнитьФункцию("ЕСЭД.ПометитьНаУдалениеИсхОтвет", ВхОбращение);
			
		КонецЕсли;
		
		Помещение = сшпОбщегоНазначения.ВыполнитьФункцию("ЕСЭД.Объект_ИД_Базис", ТелоСообщения.room_id, "Справочник.АБС_Помещения");
		
		Если Не ЗначениеЗаполнено(Помещение) Тогда 
			ВызватьИсключение "ЕСЭД.Bazis.Inspections.In: Не найдено помещение по 'room_id'";                                  
		КонецЕсли; 
		
		ЕстьАктДопускаВПомещение = ?(СвойстваСообщения.certificate_admission = "1", Истина, Ложь);
		ДатаАктаДопускаВПомещение = ?(ЗначениеЗаполнено(СвойстваСообщения.data_advp), Дата(СтрЗаменить(СвойстваСообщения.data_advp, "-", "")), ТекущаяДатаСеанса());
		
		Если ЕстьАктДопускаВПомещение Тогда 
			СтатусПередачи = Перечисления.АБС_СтатусыПередачиОбъектов.ПодписанАктДопускаВПомещение;
		Иначе
			СтатусПередачи = Перечисления.АБС_СтатусыПередачиОбъектов.ПодписанПередаточныйАктДвусторонний;
		КонецЕсли;     		
		
		Период = ?(ТелоСообщения.Свойство("updated_at") И ЗначениеЗаполнено(ТелоСообщения.updated_at), ТелоСообщения.updated_at, ТелоСообщения.created_at);  
		//сшпОбщегоНазначения.ВыполнитьФункцию("ЕСЭД.УстановитьСтатусПередачиПомещения", Помещение, СтатусПередачи);			
		сшпОбщегоНазначения.ВыполнитьФункцию("ЕСЭД.УстановитьСтатусПередачиПомещения", Помещение, СтатусПередачи, Период);	
		
		//++ @Бессчетнов.Ярослав 10.01.2024
		АПП = сшпОбщегоНазначения.ВыполнитьФункцию("ЕСЭД.АПППоПомещению", Помещение);
		
		Если ЗначениеЗаполнено(АПП) Тогда 
			
			АППОб = АПП.ПолучитьОбъект();
			
			ДатаДокументаСтарая = Формат(АППОб.АБС_ДатаДокумента, "ДФ=dd.MM.yyyy");
			ДатаДокументаНовая = Формат(ДатаАктаДопускаВПомещение, "ДФ=dd.MM.yyyy");
			АППОб.АБС_ДатаДокумента = ДатаАктаДопускаВПомещение;  
			АППОб.Заголовок = СтрЗаменить(АППОб.Заголовок, ДатаДокументаСтарая, ДатаДокументаНовая);
			
			АППОб.Записать();
			
			//Выгрузим здания по корпусу для обновления Сроков гарантии
			Корпус = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Помещение, "Владелец.Родитель");
			
			Запрос = Новый Запрос;
			Запрос.Текст = 
				"ВЫБРАТЬ
				|	АБС_Здания.Ссылка КАК Ссылка
				|ИЗ
				|	Справочник.АБС_Здания КАК АБС_Здания
				|ГДЕ
				|	НЕ АБС_Здания.ПометкаУдаления
				|	И АБС_Здания.Родитель = &Корпус";
			
			Запрос.УстановитьПараметр("Корпус", Корпус);
			
			РезультатЗапроса = Запрос.Выполнить();
			
			Выборка = РезультатЗапроса.Выбрать();
			
			Пока Выборка.Следующий() Цикл
				сшпОбработкаСобытий.ВыполнитьОбменСШП(Выборка.Ссылка.ПолучитьОбъект(), Ложь);
			КонецЦикла;	
			
		КонецЕсли;
		//-- @Бессчетнов.Ярослав 10.01.2024          
				
	ИначеЕсли ТелоСообщения.status_id = Статусы.НеПринята Тогда 
		
		ВхОбращение = сшпОбщегоНазначения.ВыполнитьФункцию("ЕСЭД.Объект_ИД_Базис", ТелоСообщения.id, "Справочник.ВходящиеДокументы");
		
		Если ЗначениеЗаполнено(ВхОбращение) Тогда 
		
			ВхОбращениеОб = ВхОбращение.ПолучитьОбъект();
			ВхОбращениеОб.АБС_Статус = ПредопределенноеЗначение("Справочник.АБС_СтатусыОбращений.Отклонен");
			ВхОбращениеОб.АБС_СпособОтвета = ПредопределенноеЗначение("Перечисление.АБС_СпособПредоставленияОтвета.ОтветНеТребуется");
			ВхОбращениеОб.Записать();
			
			сшпОбщегоНазначения.ВыполнитьФункцию("ЕСЭД.ПометитьНаУдалениеИсхОтвет", ВхОбращение);
			
		КонецЕсли;		
		
	ИначеЕсли ТелоСообщения.status_id = Статусы.Отменена Тогда 
		
		ВхОбращение = сшпОбщегоНазначения.ВыполнитьФункцию("ЕСЭД.Объект_ИД_Базис", ТелоСообщения.id, "Справочник.ВходящиеДокументы");
		
		Если Не ЗначениеЗаполнено(ВхОбращение) Тогда 
			Перейти ~Выход;			
		КонецЕсли;
		
		ВхОбращениеОб = ВхОбращение.ПолучитьОбъект();
		ВхОбращениеОб.АБС_Статус = ПредопределенноеЗначение("Справочник.АБС_СтатусыОбращений.Отклонен");
		ВхОбращениеОб.Записать();
		
		сшпОбщегоНазначения.ВыполнитьФункцию("ЕСЭД.ПометитьНаУдалениеИсхОтвет", ВхОбращение);
		
	КонецЕсли; 

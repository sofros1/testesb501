РезультатОбработки.ClassId = 130;

Properties = Новый Структура;

Properties.Вставить("access_token", ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Справочники.АБС_ДанныеДляПодключенияКИС.Базис, "ПарольБазы"));
Properties.Вставить("to_bazis", Истина);
Properties.Вставить("bazis_id", ОбъектОбработки.id);

РезультатОбработки.Properties = Properties;

РезультатОбработки.Body = сшпОбщегоНазначения.ПреобразоватьСтруктуруПоФормату(ФорматСообщения, ОбъектОбработки);

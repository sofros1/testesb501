ФорматСообщения = ПредопределенноеЗначение("Перечисление.сшпФорматыСообщений.JSON");

CreationTime = ОбъектСообщение.CreationTime;
Если Не СтрЗаканчиваетсяНа(CreationTime, "Z") Тогда
	CreationTime = CreationTime + "Z";
КонецЕсли;

ДатаСообщения = ПрочитатьДатуJSON(CreationTime, ФорматДатыJSON.ISO);

xdtoОбъект = сшпОбщегоНазначения.ПолучитьОбъектXDTO(ФорматСообщения, ОбъектСообщение.Body);

Токен = Справочники.АБС_Токены.НайтиПоНаименованию("MyABS", Истина);
Если Токен.Пустая() Тогда
	ВызватьИсключение "Не найден элемент справочника Токены (АБС) по наименованию ""MyABS""";
КонецЕсли;

token = xdtoОбъект;

ТокенОб = Токен.ПолучитьОбъект();

ТокенОб.DateToken = ДатаСообщения;
ТокенОб.accessToken = token.accessToken;
ТокенОб.refreshToken = token.refreshToken;

ТокенОб.expire_In = ПрочитатьДатуJSON(token.expire_In, ФорматДатыJSON.ISO);

ТокенОб.ДополнительныеСвойства.Вставить("СШПНеобрабатывать", Истина);
ТокенОб.Записать();

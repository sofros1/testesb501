Результат = сшпОбщегоНазначения.ВыполнитьФункцию("БП.ТаблицаСоответствийКлассовИОбъектов");

//Документы
Запись = Результат.Добавить();
Запись.ТипОбъекта = "Документ.УПН_ЗаключениеДоговора";
Запись.Класс = 110;

Запись = Результат.Добавить();
Запись.ТипОбъекта = "Документ.СРМ_Заявка";
Запись.Класс = 111;

//Справочники
Запись = Результат.Добавить();
Запись.ТипОбъекта = "Справочник.КВП_Здания";
Запись.Класс = 105;

Запись = Результат.Добавить();
Запись.ТипОбъекта = "Справочник.КВП_Подъезды";
Запись.Класс = 106;

Запись = Результат.Добавить();
Запись.ТипОбъекта = "Справочник.АБС_Этажи";
Запись.Класс = 107;

Запись = Результат.Добавить();
Запись.ТипОбъекта = "Справочник.УПЖКХ_Помещения";
Запись.Класс = 108;

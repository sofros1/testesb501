	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ЗакрепленныеСчетчики.Объект КАК ЛицевойСчет,
	|	ЗакрепленныеСчетчики.Счетчик КАК Счетчик
	|ИЗ
	|	(ВЫБРАТЬ
	|		ЗакрепленныеСчетчики.Счетчик КАК Счетчик,
	|		МАКСИМУМ(ЗакрепленныеСчетчики.ДатаИзменения) КАК ДатаИзменения
	|	ИЗ
	|		РегистрСведений.КВП_ЗакрепленныеСчетчики КАК ЗакрепленныеСчетчики
	|	ГДЕ
	|		ЗакрепленныеСчетчики.Счетчик = &Счетчик
	|		И ЗакрепленныеСчетчики.Действует
	|	
	|	СГРУППИРОВАТЬ ПО
	|		ЗакрепленныеСчетчики.Счетчик) КАК ВложенныйЗапрос
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.КВП_ЗакрепленныеСчетчики КАК ЗакрепленныеСчетчики
	|		ПО ВложенныйЗапрос.Счетчик = ЗакрепленныеСчетчики.Счетчик
	|			И ВложенныйЗапрос.ДатаИзменения = ЗакрепленныеСчетчики.ДатаИзменения
	|ГДЕ
	|	ЗакрепленныеСчетчики.Действует
	|	И ЗакрепленныеСчетчики.Объект ССЫЛКА Справочник.КВП_ЛицевыеСчета";  
	
	Запрос.УстановитьПараметр("Счетчик", Счетчик);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Выборка = РезультатЗапроса.Выбрать();
	
	Если Выборка.Следующий() Тогда 
		Результат = Выборка.ЛицевойСчет;
	Иначе
		Результат = Неопределено;
	КонецЕсли;

	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ВложенныйЗапрос.ЛицевойСчет КАК ЛицевойСчет,
		|	ВложенныйЗапрос.ФизЛицо КАК ФизЛицо,
		|	СУММА(ВложенныйЗапрос.Собственник) КАК Собственник,
		|	СУММА(ВложенныйЗапрос.Прописан) КАК Прописан,
		|	СУММА(ВложенныйЗапрос.Зарегистрирован) КАК Зарегистрирован,
		|	СУММА(ВложенныйЗапрос.Ответственный) КАК Ответственный
		|ПОМЕСТИТЬ втЖильцы
		|ИЗ
		|	(ВЫБРАТЬ
		|		СведенияОЗарегистрированных.ЛицевойСчет КАК ЛицевойСчет,
		|		СведенияОЗарегистрированных.Жилец.ФизЛицо КАК ФизЛицо,
		|		0 КАК Собственник,
		|		ВЫБОР
		|			КОГДА СведенияОЗарегистрированных.Зарегистрирован
		|					И НЕ СведенияОЗарегистрированных.ВременнаяРегистрация
		|				ТОГДА 1
		|			ИНАЧЕ 0
		|		КОНЕЦ КАК Прописан,
		|		ВЫБОР
		|			КОГДА СведенияОЗарегистрированных.ВременнаяРегистрация
		|				ТОГДА 1
		|			ИНАЧЕ 0
		|		КОНЕЦ КАК Зарегистрирован,
		|		0 КАК Ответственный
		|	ИЗ
		|		РегистрСведений.УПЖКХ_СведенияОЗарегистрированных.СрезПоследних(
		|				,
		|				ЛицевойСчет = &ЛицевойСчет
		|					И ДатаИзменения <= &ТекущаяДата) КАК СведенияОЗарегистрированных
		|	
		|	ОБЪЕДИНИТЬ ВСЕ
		|	
		|	ВЫБРАТЬ
		|		Жильцы.Владелец,
		|		СобственникиПомещений.Собственник,
		|		ВЫБОР
		|			КОГДА СобственникиПомещений.Действует
		|				ТОГДА 1
		|			ИНАЧЕ 0
		|		КОНЕЦ,
		|		0,
		|		0,
		|		0
		|	ИЗ
		|		РегистрСведений.УПЖКХ_СобственникиПомещений.СрезПоследних(, Собственник ССЫЛКА Справочник.ФизическиеЛица) КАК СобственникиПомещений
		|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.УПЖКХ_Жильцы КАК Жильцы
		|			ПО СобственникиПомещений.Собственник = Жильцы.ФизЛицо
		|				И СобственникиПомещений.Помещение = Жильцы.Владелец.Адрес
		|	ГДЕ
		|		Жильцы.Владелец = &ЛицевойСчет
		|	
		|	ОБЪЕДИНИТЬ ВСЕ
		|	
		|	ВЫБРАТЬ
		|		ОтветственныйСобственникНанимательЛицевогоСчета.ЛицевойСчет,
		|		СоответствиеКонтрагентовФизлиц.ФизЛицо,
		|		0,
		|		0,
		|		0,
		|		1
		|	ИЗ
		|		РегистрСведений.УПЖКХ_ОтветственныйСобственникНанимательЛицевогоСчета.СрезПоследних(, ЛицевойСчет = &ЛицевойСчет) КАК ОтветственныйСобственникНанимательЛицевогоСчета
		|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.АБС_СоответствиеКонтрагентовФизлиц КАК СоответствиеКонтрагентовФизлиц
		|			ПО ОтветственныйСобственникНанимательЛицевогоСчета.ОтветственныйВладелец = СоответствиеКонтрагентовФизлиц.Контрагент) КАК ВложенныйЗапрос
		|
		|СГРУППИРОВАТЬ ПО
		|	ВложенныйЗапрос.ЛицевойСчет,
		|	ВложенныйЗапрос.ФизЛицо
		|
		|ИМЕЮЩИЕ
		|	(СУММА(ВложенныйЗапрос.Собственник) = 1
		|		ИЛИ СУММА(ВложенныйЗапрос.Прописан) = 1
		|		ИЛИ СУММА(ВложенныйЗапрос.Зарегистрирован) = 1
		|		ИЛИ СУММА(ВложенныйЗапрос.Ответственный) = 1)
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВложенныйЗапрос.ЛицевойСчет КАК ЛицевойСчет,
		|	СтатусыДебиторскойЗадолженности.СтатусДебиторскойЗадолженности КАК Статус
		|ПОМЕСТИТЬ втСтатусы
		|ИЗ
		|	(ВЫБРАТЬ
		|		СтатусыДебиторскойЗадолженности.Сессия.ЛицевойСчет КАК ЛицевойСчет,
		|		МАКСИМУМ(СтатусыДебиторскойЗадолженности.Период) КАК Период
		|	ИЗ
		|		РегистрСведений.АБС_СтатусыДебиторскойЗадолженности КАК СтатусыДебиторскойЗадолженности
		|	ГДЕ
		|		СтатусыДебиторскойЗадолженности.Сессия.ЛицевойСчет = &ЛицевойСчет
		|	
		|	СГРУППИРОВАТЬ ПО
		|		СтатусыДебиторскойЗадолженности.Сессия.ЛицевойСчет) КАК ВложенныйЗапрос
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.АБС_СтатусыДебиторскойЗадолженности КАК СтатусыДебиторскойЗадолженности
		|		ПО ВложенныйЗапрос.ЛицевойСчет = СтатусыДебиторскойЗадолженности.Сессия.ЛицевойСчет
		|			И ВложенныйЗапрос.Период = СтатусыДебиторскойЗадолженности.Период
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ЛицевыеСчета.Ссылка КАК ЛицевойСчет,
		|	МАКСИМУМ(ЛицевыеСчета.Идентификатор) КАК НомерСчета,
		|	МАКСИМУМ(ЛицевыеСчета.Адрес) КАК Помещение,
		|	втЖильцы.ФизЛицо КАК ФизЛицо,
		|	МАКСИМУМ(ЛицевыеСчетаСрезПервых.Период) КАК ДатаОткрытияСчета,
		|	МАКСИМУМ(ЕСТЬNULL(ЛицевыеСчетаСрезПоследних.Период, ДАТАВРЕМЯ(1, 1, 1))) КАК ДатаЗакрытияСчета,
		|	МАКСИМУМ(ВЫБОР
        |   	КОГДА  ЛицевыеСчетаСрезПоследних.Период ЕСТЬ NULL или ДОБАВИТЬКДАТЕ(ЛицевыеСчетаСрезПоследних.Период, ДЕНЬ, 60) >= &ТекущаяДата
        |       	ТОГДА ИСТИНА
        | 		ИНАЧЕ ЛОЖЬ
        |    КОНЕЦ) КАК Действует,	
		|	МАКСИМУМ(ЕСТЬNULL(втСтатусы.Статус, """")) КАК Статус,
		|	МАКСИМУМ(ЛицевыеСчета.ЗарегистрированВУЖКХ) КАК ЗарегистрированВУЖКХ,
		|	СУММА(втЖильцы.Зарегистрирован) КАК Зарегистрирован,
		|	СУММА(втЖильцы.Прописан) КАК Прописан,
		|	СУММА(втЖильцы.Собственник) КАК Собственник,
		|	СУММА(втЖильцы.Ответственный) КАК Ответственный
		|ИЗ
		|	Справочник.КВП_ЛицевыеСчета КАК ЛицевыеСчета
		|		ЛЕВОЕ СОЕДИНЕНИЕ втЖильцы КАК втЖильцы
		|		ПО ЛицевыеСчета.Ссылка = втЖильцы.ЛицевойСчет
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.КВП_ЛицевыеСчета.СрезПервых(
		|				,
		|				ЛицевойСчет = &ЛицевойСчет
		|					И Регистратор ССЫЛКА Документ.КВП_ОткрытиеЛицевогоСчета
		|					И Действует) КАК ЛицевыеСчетаСрезПервых
		|		ПО ЛицевыеСчета.Ссылка = ЛицевыеСчетаСрезПервых.ЛицевойСчет
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.КВП_ЛицевыеСчета.СрезПоследних(
		|				,
		|				ЛицевойСчет = &ЛицевойСчет
		|					И Регистратор ССЫЛКА Документ.КВП_ЗакрытиеЛицевогоСчета
		|					И НЕ Действует) КАК ЛицевыеСчетаСрезПоследних
		|		ПО ЛицевыеСчета.Ссылка = ЛицевыеСчетаСрезПоследних.ЛицевойСчет
		|		ЛЕВОЕ СОЕДИНЕНИЕ втСтатусы КАК втСтатусы
		|		ПО ЛицевыеСчета.Ссылка = втСтатусы.ЛицевойСчет
		|ГДЕ
		|	ЛицевыеСчета.Ссылка = &ЛицевойСчет
		|
		|СГРУППИРОВАТЬ ПО
		|	втЖильцы.ФизЛицо,
		|	ЛицевыеСчета.Ссылка
		|ИТОГИ
		|	МАКСИМУМ(НомерСчета),
		|	МАКСИМУМ(Помещение),
		|	МАКСИМУМ(ДатаОткрытияСчета),
		|	МАКСИМУМ(ДатаЗакрытияСчета),
		|	МАКСИМУМ(Действует),
		|	МАКСИМУМ(Статус),
		|	МАКСИМУМ(ЗарегистрированВУЖКХ),
		|	СУММА(Зарегистрирован),
		|	СУММА(Прописан),
		|	СУММА(Собственник),
		|	СУММА(Ответственный)
		|ПО
		|	ЛицевойСчет";
	
	Запрос.УстановитьПараметр("ЛицевойСчет", ЛицевойСчет);
	Запрос.УстановитьПараметр("ТекущаяДата", ТекущаяДатаСеанса());
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаЛицевойСчет = РезультатЗапроса.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
	ВыборкаЛицевойСчет.Следующий();
			
	Результат = Новый Структура();
	Результат.Вставить("Ссылка", ВыборкаЛицевойСчет.ЛицевойСчет);
	Результат.Вставить("НомерСчета", ВыборкаЛицевойСчет.НомерСчета);
	Результат.Вставить("Помещение", ВыборкаЛицевойСчет.Помещение);
	Результат.Вставить("Действует", ВыборкаЛицевойСчет.Действует);
	Результат.Вставить("ДатаОткрытияСчета", ВыборкаЛицевойСчет.ДатаОткрытияСчета);
	Результат.Вставить("ДатаЗакрытияСчета", ВыборкаЛицевойСчет.ДатаЗакрытияСчета);
	Результат.Вставить("КоличествоЗарегистрировано", ВыборкаЛицевойСчет.Зарегистрирован);
	Результат.Вставить("КоличествоПрописано", ВыборкаЛицевойСчет.Прописан);
	Результат.Вставить("КоличествоСобственников", ВыборкаЛицевойСчет.Собственник); 
	Результат.Вставить("ЗарегистрированВУЖКХ", ВыборкаЛицевойСчет.ЗарегистрированВУЖКХ);
	Результат.Вставить("Статус", ВыборкаЛицевойСчет.Статус);
	Статус = ВыборкаЛицевойСчет.Статус; //Для того, чтобы не было ошибки "Поле объекта недоступно для записи (Статус)"
	Результат.Вставить("СтатусУЖКХ", сшпОбщегоНазначения.ВыполнитьФункцию("ЖКХ.СоответствиеСтатусовЛицевыхСчетов", Статус));
			
	Жильцы = Новый Массив;
	
	ВыборкаЖильцы = ВыборкаЛицевойСчет.Выбрать();
	
	Пока ВыборкаЖильцы.Следующий() Цикл
		
		Жилец = Новый Структура();
		Жилец.Вставить("Ссылка", ВыборкаЖильцы.ФизЛицо);
		Жилец.Вставить("Собственник", ?(ВыборкаЖильцы.Собственник = 1, Истина, Ложь));
		Жилец.Вставить("Прописан", ?(ВыборкаЖильцы.Прописан = 1, Истина, Ложь));
		Жилец.Вставить("Зарегистрирован", ?(ВыборкаЖильцы.Зарегистрирован = 1, Истина, Ложь));
		Жилец.Вставить("Арендатор", Ложь); //Доработать!
		
		Жильцы.Добавить(Жилец);
		
	КонецЦикла;
	
	Результат.Вставить("Жильцы", Жильцы);
	
	//Баланс
	ДанныеБаланса = crm_ObmenGKHCRM.ПолучитьБалансЛС(Новый Структура("ЛицевыеСчета", ЛицевойСчет));
	Результат.Вставить("Баланс", -ДанныеБаланса.СуммаБаланса);
	Если Результат.Баланс >= 0 Тогда
		Результат.СтатусУЖКХ = "Задолженности нет";
	КонецЕсли;
	
	//++ Марина Топталина 10.11.2023 [BB-10466]
	//Адрес
	Здание = ВыборкаЛицевойСчет.Помещение.Владелец;
	ТЗ_КИ   = Здание.КонтактнаяИнформация.Выгрузить();

    //почтовый адрес
    Отбор = Новый Структура;
    Отбор.Вставить("Вид", ПредопределенноеЗначение("Справочник.ВидыКонтактнойИнформации.УПЖКХ_ПочтовыйАдресЗдания"));
    ПочтовыйАдрес = ТЗ_КИ.НайтиСтроки(Отбор);

    Если ПочтовыйАдрес.Количество() > 0
    	И Не ПустаяСтрока(ПочтовыйАдрес[0].Значение) Тогда
	
	    СтруктураАдресаЗдания = АБС_JSON._ПрочитатьJSON(ПочтовыйАдрес[0].Значение); 

	    Адрес = СтрШаблон("%1%2, %3. %4", 
			?(СтруктураАдресаЗдания["streetType"]="ул",СтруктураАдресаЗдания["streetType"]+". ",""),
			СтруктураАдресаЗдания["street"],
	        Сред(НРег(СтруктураАдресаЗдания["houseType"]),1,1),
	        СтруктураАдресаЗдания["houseNumber"]);	
    Иначе    
	    Адрес = "";	    
    КонецЕсли;
    
    Результат.Вставить("Адрес",Адрес);
    //-- Марина Топталина 10.11.2023 [BB-10466]
	

	//Из строки вида "2 октября 2022 00:00"
 	ЧД = СтрРазделить(СокрЛП(ДатаСтрокой), " ", Ложь);
    Если ЧД.Количество() = 4 Тогда
    
            День = Число(СокрЛП(ЧД[0]));
			
            Месяцы = Новый Массив();
            Месяцы.Добавить("ЯНВАРЯ"); 	Месяцы.Добавить("ФЕВРАЛЯ"); 		Месяцы.Добавить("МАРТА");
            Месяцы.Добавить("АПРЕЛЯ"); 	Месяцы.Добавить("МАЯ"); 				Месяцы.Добавить("ИЮНЯ");
            Месяцы.Добавить("ИЮЛЯ"); 		Месяцы.Добавить("АВГУСТА"); 		Месяцы.Добавить("СЕНТЯБРЯ");
            Месяцы.Добавить("ОКТЯБРЯ"); 	Месяцы.Добавить("НОЯБРЯ");			Месяцы.Добавить("ДЕКАБРЯ");
            Месяц = Месяцы.Найти(Врег(СокрЛП(ЧД[1]))) + 1;
			
            Год = Число(СокрЛП(ЧД[2]));
			
			ВремяСтрокой = ЧД[3];
			
	        Результат = Дата(Год, Месяц, День, Лев(ВремяСтрокой, 2), Прав(ВремяСтрокой, 2), 0);
	   
    Иначе
    	Результат = Дата(1, 1, 1);    
    КонецЕсли;
  
    

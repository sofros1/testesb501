
Попытка
	ТипПереданногоЗначения = Тип(СтруктураСсылки.Тип);
Исключение
	ТипПереданногоЗначения = Тип("Неопределено");
КонецПопытки;

ЗначениеСтрокой = Строка(СтруктураСсылки.Значение);

Если ТипПереданногоЗначения = ТипЗнч(Неопределено) Тогда 
	
	Результат = Неопределено;
	
ИначеЕсли ОбщегоНазначения.ЭтоСсылка(ТипПереданногоЗначения) Тогда 
	
	ПустаяСсылкаПереданногоТипа = Новый(ТипПереданногоЗначения);
	МетаданныеПустойСсылкиПереданногоТипа = ПустаяСсылкаПереданногоТипа.Метаданные();
	
	Если ПустаяСтрока(ЗначениеСтрокой) Тогда
		
		Результат = ПустаяСсылкаПереданногоТипа;
		
	ИначеЕсли ОбщегоНазначения.ЭтоПеречисление(МетаданныеПустойСсылкиПереданногоТипа) Тогда
		
		Результат = XMLЗначение(ТипПереданногоЗначения, ЗначениеСтрокой);
		
	ИначеЕсли ПустаяСтрока(ЗначениеСтрокой)
		Или ЗначениеСтрокой = Строка(ПустаяСсылкаПереданногоТипа.УникальныйИдентификатор()) Тогда
		
		Результат = ПустаяСсылкаПереданногоТипа;
		
	Иначе
		
		ИмяМенеджера = МетаданныеПустойСсылкиПереданногоТипа.ПолноеИмя();
		КлассПакетаСсылки = сткСообщение.КлассыОбъектов.Получить(ИмяМенеджера);
		
		Если КлассПакетаСсылки = Неопределено Тогда

			Если НеВызыватьИсключениеПоКлассу = Истина Тогда
				
				Результат = ПустаяСсылкаПереданногоТипа;
				
			Иначе
				
				ВызватьИсключение СтрШаблон("Отсутствует описание соответствия класса типу для: ""%1""", ИмяМенеджера);
				
			КонецЕсли;
			
		Иначе
		
			СвойствоПредопределенныхДанных = СтруктураСсылки.Свойства().Получить("ИмяПредопределенныхДанных");
			Если СвойствоПредопределенныхДанных <> Неопределено Тогда
			
				ИмяПредопределенныхДанных = СтруктураСсылки.Получить(СвойствоПредопределенныхДанных);
			
			Иначе
			
				ИмяПредопределенныхДанных = "";
			
			КонецЕсли;
		
			Если Не ПустаяСтрока(ИмяПредопределенныхДанных) Тогда
				
				МенеджерОбъектаСсылки = ОбщегоНазначения.МенеджерОбъектаПоПолномуИмени(ИмяМенеджера);
				
				Попытка 
					Результат = МенеджерОбъектаСсылки[ИмяПредопределенныхДанных];
				Исключение
				КонецПопытки;
			
			КонецЕсли;
			
			Если Результат = Неопределено Тогда
				Результат = сшпИнтеграцияСервер.ПолучитьСсылку(ЗначениеСтрокой, ИмяМенеджера, КлассПакетаСсылки, сткСообщение);
			КонецЕсли;
			
		КонецЕсли;
		
	КонецЕсли;
	
Иначе
	
	Результат = XMLЗначение(ТипПереданногоЗначения, ЗначениеСтрокой);
	
КонецЕсли;

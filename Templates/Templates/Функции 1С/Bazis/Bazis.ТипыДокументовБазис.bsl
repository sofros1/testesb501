ТипыДокументов = Новый Соответствие();

//Типы промежуточных документов (обязательно в Справочник.ЗначенияСвойствОбъектов должен быть элемент с таким же наименованием и родителем "Типы промежуточных файлов")
ТипыДокументов.Вставить(265, "Уведомление о предоставлении доступа");
ТипыДокументов.Вставить(266, "Повторное уведомление о предоставлении доступа");
ТипыДокументов.Вставить(267, "Уведомление о невозможности выполнения обязательств");
ТипыДокументов.Вставить(234, "7_Акт комиссионного обследования");
ТипыДокументов.Вставить(235, "8_Акт устранения недостатков");
ТипыДокументов.Вставить(269, "Акт об отсутствии доступа");
ТипыДокументов.Вставить(270, "Вызов на рекламацию");
ТипыДокументов.Вставить(271, "Рекламационный акт");

//Остальные
ТипыДокументов.Вставить(262, "Акт осмотра без замечаний");
ТипыДокументов.Вставить(263, "Односторонний акт осмотра без замечаний");

Результат = ТипыДокументов;

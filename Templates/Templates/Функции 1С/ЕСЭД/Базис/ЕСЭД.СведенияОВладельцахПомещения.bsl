		Запрос = Новый Запрос;
	
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	АБС_СведенияОВладельцахПомещений.Корреспондент КАК Заявитель,
		|	АБС_СведенияОВладельцахПомещений.Корреспондент.ФизЛицо КАК ЗаявительФизЛицо
		|ИЗ
		|	РегистрСведений.АБС_СведенияОВладельцахПомещений КАК АБС_СведенияОВладельцахПомещений
		|ГДЕ
		|	АБС_СведенияОВладельцахПомещений.Помещение = &Помещение
		|	И АБС_СведенияОВладельцахПомещений.Собственник";
		
		Запрос.УстановитьПараметр("Помещение", Помещение);
		
		РезультатЗапроса = Запрос.Выполнить();
		
		Результат = РезультатЗапроса.Выгрузить();  	

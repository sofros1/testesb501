Запрос = Новый Запрос;
Запрос.Текст = 
"ВЫБРАТЬ РАЗРЕШЕННЫЕ
|	ФайлыДополнительныеРеквизиты.Ссылка.ВладелецФайла КАК Предписание
|ИЗ
|	Справочник.Файлы.ДополнительныеРеквизиты КАК ФайлыДополнительныеРеквизиты
|ГДЕ
|	ФайлыДополнительныеРеквизиты.Ссылка = &ФайлПечатнойФормы
|	И ФайлыДополнительныеРеквизиты.Свойство.Имя = ""ТипПечатнойФормы""
|	И ФайлыДополнительныеРеквизиты.Значение = ""Предписание""";

Запрос.УстановитьПараметр("ФайлПечатнойФормы", ФайлПечатнойФормы);

РезультатЗапроса = Запрос.Выполнить();

Если Не РезультатЗапроса.Пустой() Тогда
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	ВыборкаДетальныеЗаписи.Следующий();
	
	МенеджерПВХ = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения;
	РеквизитПВХ = МенеджерПВХ.НайтиПоРеквизиту("Имя", "СогласиеПодрядчикаСЗамечаниями");
	
	ЗначениеСогласен = Справочники.ЗначенияСвойствОбъектов.НайтиПоНаименованию("Согласен", Истина, , РеквизитПВХ);
	Если ЗначениеЗаполнено(ЗначениеСогласен) Тогда
		
		ПредписаниеОбъект = ВыборкаДетальныеЗаписи.Предписание.ПолучитьОбъект();
		
		Найден = Ложь;
		
		Для Каждого СтрРеквизита из ПредписаниеОбъект.ДополнительныеРеквизиты Цикл
			
			Если СтрРеквизита.Свойство = РеквизитПВХ Тогда 
				
				СтрРеквизита.Значение  = ЗначениеСогласен;
				Найден = Истина;
				
				Прервать;
				
			КонецЕсли;
			
		КонецЦикла;
		
		Если Не Найден Тогда
			
			СтрРеквизита = ПредписаниеОбъект.ДополнительныеРеквизиты.Добавить();
			СтрРеквизита.Свойство = РеквизитПВХ;
			СтрРеквизита.Значение  = ЗначениеСогласен;
			
		КонецЕсли;
			
		ОбновлениеИнформационнойБазы.ЗаписатьДанные(ПредписаниеОбъект);	
		
	КонецЕсли; 
	
КонецЕсли;

Результат = Истина;

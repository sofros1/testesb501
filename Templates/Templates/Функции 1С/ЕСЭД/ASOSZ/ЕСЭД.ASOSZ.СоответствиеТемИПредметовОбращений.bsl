	ПредметыОбращений = АБС_СвойстваПовтИсп.ПолучитьСоответствиеСсылокНаименованиям("
		|АБС_ПредметыОбращений.НаличиеНедостатковДефектов;
		|АБС_ПредметыОбращений.УступкаПраваТребованияСменаПравообладателя;
		|АБС_ПредметыОбращений.ОбращениеКЗастройщикуПоДругимВопросам;
		|АБС_ПредметыОбращений.НарушениеСрокаПередачиОбъектаДолевогоСтроительства;
		|АБС_ПредметыОбращений.ПредоставлениеКопийДокументов;
		|АБС_ПредметыОбращений.ЖалобаНаСотрудника;
		|АБС_ПредметыОбращений.БлагодарностьСотруднику;
		|АБС_ПредметыОбращений.УведомлениеЗастройщикаОПереуступкеПраваТребования");	
	
	Соответствие = Новый Соответствие;
	Соответствие.Вставить(1, ПредметыОбращений["АБС_ПредметыОбращений.НаличиеНедостатковДефектов"]);
	Соответствие.Вставить(2, ПредметыОбращений["АБС_ПредметыОбращений.УступкаПраваТребованияСменаПравообладателя"]);
	Соответствие.Вставить(3, ПредметыОбращений["АБС_ПредметыОбращений.УведомлениеЗастройщикаОПереуступкеПраваТребования"]);
	Соответствие.Вставить(4, ПредметыОбращений["АБС_ПредметыОбращений.ПредоставлениеКопийДокументов"]);
	Соответствие.Вставить(5, ПредметыОбращений["АБС_ПредметыОбращений.НарушениеСрокаПередачиОбъектаДолевогоСтроительства"]);
	Соответствие.Вставить(6, ПредметыОбращений["АБС_ПредметыОбращений.ЖалобаНаСотрудника"]);
	Соответствие.Вставить(7, ПредметыОбращений["АБС_ПредметыОбращений.БлагодарностьСотруднику"]);
	Соответствие.Вставить(8, ПредметыОбращений["АБС_ПредметыОбращений.ОбращениеКЗастройщикуПоДругимВопросам"]);
	Соответствие.Вставить(9, ПредметыОбращений["АБС_ПредметыОбращений.ОбращениеКЗастройщикуПоДругимВопросам"]);
	
	Результат = Соответствие[ТемаОбращения];

		

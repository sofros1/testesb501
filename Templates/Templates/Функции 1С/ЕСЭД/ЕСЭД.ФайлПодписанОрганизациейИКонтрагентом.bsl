ИнформацияОЭПФайла = Новый Структура("ПодписанОрганизацией, ПодписанКонрагентом", Ложь, Ложь);
	
Если ДанныеФайла.ПодписанЭП Тогда
	
	ВыборкаДетальныеЗаписи = РаботаСЭП.ПолучитьЭлектронныеПодписи(ДанныеФайла.ТекущаяВерсияФайла);
	
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		
		ОписаниеЭП = РаботаСЭП.ОписаниеЭПДляОтметки(ВыборкаДетальныеЗаписи);
		
		Если Не ИнформацияОЭПФайла.ПодписанОрганизацией Тогда
			ИнформацияОЭПФайла.ПодписанОрганизацией = ОписаниеЭП.ПодписьОрганизации;
		КонецЕсли;
		
		Если Не ИнформацияОЭПФайла.ПодписанКонрагентом Тогда 
			ИнформацияОЭПФайла.ПодписанКонрагентом = Не ОписаниеЭП.ПодписьОрганизации;
		КонецЕсли;
		
	КонецЦикла;	
	
КонецЕсли;

Результат = ИнформацияОЭПФайла;

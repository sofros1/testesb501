Результат = Ложь;

Идентификатор = Новый УникальныйИдентификатор;
ИдентификаторСобытия = Новый УникальныйИдентификатор;
ДатаРегистрации = ТекущаяДатаСеанса();

Если ФорматСообщения = Неопределено Тогда
	ФорматСообщения = сшпФункциональныеОпции.ФорматСообщения();
КонецЕсли;

ВерсияОбработчиков = сшпФункциональныеОпции.ВерсияОбработчиков();

МетодХранения = сшпКэшируемыеФункции.ПолучитьМетодХранения(ТипОбъекта, ВерсияОбработчиков);
СткОбработчик = сшпКэшируемыеФункции.ПолучитьОбработчик(ТипОбъекта, ПредопределенноеЗначение("Перечисление.сшпТипыИнтеграции.Исходящая"), ВерсияОбработчиков);

Если Не ЗначениеЗаполнено(СткОбработчик.ПроцедураОбработки) Тогда
	
	ТекстОшибки = "В 1С отсутствует исходящий обработчик для " + Строка(ТипОбъекта);
	
	Перейти ~Выход;
	
КонецЕсли;

Если СткОбработчик.Статус = Перечисления.сшпСтатусыОбработчиков.Отключен Тогда
	
	ТекстОшибки = "Обработка исходящего сообщения была отменена! Обработчик отключен!";
	
	Перейти ~Выход;
	
КонецЕсли;

РезультатОбработки = сшпОбщегоНазначения.СформироватьСтруктуруПакета();
РезультатОбработки.Id = Идентификатор;

Попытка
	
	СостояниеСообщения = Перечисления.сшпСтатусыСообщений.Обработано;
	ОбъектОбработки = Неопределено;
	
	Если МетодХранения = Перечисления.сшпМетодХранения.ПоСсылке Тогда
		
		Если ТипЗнч(ОбъектСобытия) = Тип("Отбор") Тогда
			
			ТипРегистра = сшпКэшируемыеФункции.ПолучитьТипОбъекта(ТипОбъекта);
			ИмяРегистра = сшпКэшируемыеФункции.ПолучитьИмяОбъекта(ТипОбъекта);
			
			Если ТипРегистра = "РегистрСведений" Тогда
				
				ОбъектОбработки = РегистрыСведений[ИмяРегистра].СоздатьНаборЗаписей();	
				
			ИначеЕсли ТипРегистра = "РегистрНакопления" Тогда
				
				ОбъектОбработки = РегистрыНакопления[ИмяРегистра].СоздатьНаборЗаписей();
				
			ИначеЕсли ТипРегистра = "РегистрБухгалтерии" Тогда
				
				ОбъектОбработки = РегистрыБухгалтерии[ИмяРегистра].СоздатьНаборЗаписей();
				
			ИначеЕсли ТипРегистра = "РегистрРасчета" Тогда
				
				ОбъектОбработки = РегистрыРасчета[ИмяРегистра].СоздатьНаборЗаписей();
				
			Иначе
				
				ВызватьИсключение "Тип: " + ТипРегистра + " не поддерживается текущей версией подсистемы ESB";
				
			КонецЕсли;
			
			Для Каждого ЭлементОтбор Из ОбъектСобытия Цикл
				
				ЗаполнитьЗначенияСвойств(ОбъектОбработки.Отбор[ЭлементОтбор.Имя], ЭлементОтбор);
				
			КонецЦикла;
			
			ОбъектОбработки.Прочитать();
			
		Иначе
			
			ОбъектОбработки = ОбъектСобытия;
			
		КонецЕсли;
		
	Иначе
		
		ОбъектОбработки = сшпОбщегоНазначения.ДесериализоватьОбъект(ФорматСообщения, ОбъектСобытия);
		
	КонецЕсли;
	
	Задержка = 0;
	
	сшпОбщегоНазначения.ВыполнитьКодИсходящегоОбработчика(сткОбработчик.ПроцедураОбработки,
			РезультатОбработки, Идентификатор, ИдентификаторСобытия, ТипОбъекта, МетодХранения,
			ОбъектСобытия, Задержка, СткОбработчик, ФорматСообщения, ТекстОшибки, ДатаРегистрации,
			ЭтоУдаление, СсылкаНаОбъект, ОбъектОбработки, СостояниеСообщения);
	
	Если СостояниеСообщения = Перечисления.сшпСтатусыСообщений.Обработано Тогда
		
		сшпОбщегоНазначения.УстановитьСвойствоПоиска(СсылкаНаОбъект, РезультатОбработки);
		
		Коннектор = сшпВзаимодействиеСАдаптером.ПолучитьКоннектор();
		Если Коннектор = Неопределено Тогда
			
			ВызватьИсключение "Отсутствует связь с адаптером";
			
		КонецЕсли;
		
		сшпОбслуживаниеОчередей.ОтправитьСообщение(Коннектор, РезультатОбработки);
		
	КонецЕсли;
	
Исключение
	
	ТекстОшибки = сшпОбщегоНазначения.ПолучитьТекстОшибкиОбработчика(ИнформацияОбОшибке());
	
	СостояниеСообщения = Перечисления.сшпСтатусыСообщений.ОшибкаОбработки;
	
КонецПопытки;

Если Не СостояниеСообщения = Перечисления.сшпСтатусыСообщений.Обработано Тогда
	
	ТекстОшибки = сшпОбщегоНазначения.ДополнитьТекстОшибки(ТекстОшибки, сткОбработчик, СсылкаНаОбъект);
	
	Перейти ~Выход;
	
КонецЕсли;				

Результат = Истина;

	Статусы = сшпОбщегоНазначения.ВыполнитьФункцию("СтатусыBazis", "Inspection");
	
	ЗапрашиваемыеСтатусы = Новый Массив;
	ЗапрашиваемыеСтатусы.Добавить(Статусы.Новая);
	ЗапрашиваемыеСтатусы.Добавить(Статусы.ВзятаВРаботу);
	
	Для Каждого Статус Из ЗапрашиваемыеСтатусы Цикл 
	
		Структура = Новый Структура();
		Структура.Вставить("replyMessageClass", 147); //Bazis.Attachments
		Структура.Вставить("messageType", "Inspection_embed"); 
		Структура.Вставить("url", СтрШаблон("/inspections?typeId=1&inSchedule=true&statusId=%1&embed=expert.attachments", Статус));    
		Структура.Вставить("type", "Inspection");
		Структура.Вставить("isInspectionsData", Истина);  		
		сшпПользовательскиеМетоды.ПоместитьВОчередьИсходящих("Bazis.Embed.Request", Структура); 
		
	КонецЦикла;
